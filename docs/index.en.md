# Das Gebu-Programm

![Logo – a cat](images/logo.png){ align=right }

!!! tip ""
	Never forget birthdays anymore.

The Gebu program displays birthdays in a frame of two weeks, i.e. last week's birthdays, today's birthdays and next week's future birthdays.

The birthdays can be edited with an internal editor.
The display interval is adjustable.

In the usual application scenario the Gebu program is started with the computer.
Thus the currently relevant birthdays and other events are displayed.

Use the ESC function (++esc++ key) to close the program quickly.
