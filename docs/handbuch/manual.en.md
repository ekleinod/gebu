# Usage

![Logo – a cat](../images/logo.png){ align=right }

!!! info
	On this page only the menu commands are described that have to be called.
	The corresponding shortcuts are shown in the menu or can be looked up on the [shortcuts page](../shortcuts).

!!! info
	The program is not yet translated to English.


## Starting Das Gebu-Programm

After [installation](../install) you can start Das Gebu-Programm from the start menu or it will be started automatically with the computer.

If you did neither install the program nor create a shortcut, start Das Gebu-Programm directly as described in [Installation – direct call](../install/#direct-call-of-gebujar).

On startup, the last opened file is automatically loaded and the birthday view is shown.
If no file has been opened yet, the program starts with the editor view.


## Birthday view

The birthday view shows the birthdays that are in the view interval.
It is shown at startup if a birthday file has been loaded.

The view can be customized in the settings (*Program → Einstellungen*).

Use *Program → Editor* to switch to edit mode.


## Editing

In edit mode you can edit data, create new files or view statistics.

Use *Program → Anzeige* to switch to the birthday view.

### Create new file

Create a new file via *File → Neu*.

If you have unsaved changes, save them before creating a new file.

### Open file

Opens an existing file via *File → Öffnen…*.
The last opened file will be automatically reopened when the program starts.

If you have unsaved changes, save them before opening a file.

### Viewing, entering, changing and deleting events

In edit mode, a list of all existing events is shown on the left.
On the right side resides the detail view, which shows the details for the events selected on the left.

At the bottom of the detail view are buttons for creating, editing and deleting events: ++"Neu… "++, ++"Ändern… "++ and ++"Löschen"++.
You can also open events for editing by double-clicking in the list.

When you create new events or edit existing ones, the event editor opens.
Here you can enter date, title, event type and category.

Date, title and event type are mandatory fields.

Event type and category offer existing entries for selection.
If you want to create a new event type or category, simply enter it in the field.

Accept the changes with ++"OK"++.
Discard the changes with ++"Abbrechen"++.

If you delete events, a corresponding prompt will appear, click ++"OK "++ to delete the event.

After you have entered, changed or deleted data, you must save the file.
The program does not save files automatically, but will indicate unsaved changes.

### Save data

Data is saved locally on the computer in a file.
For this purpose, a file name must be assigned when saving for the first time.

The data is not encrypted.


## Settings

In the settings (*Program → Einstellungen*) you can influence the behavior or appearance of the program.

### Hideings

You can exclude event types or categories from view.
To do this, you must check the event types or categories to be hidden.

This is useful, for example, if the same file is used on different computers and different events are to be viewed there.
For example, private birthdays can be hidden in a work environment.

### Event view

In addition to the view interval, you can specify the color and font size for past, current and future events.

### Other

Here you can set whether only the file name or the entire path is shown in the program title (default: *only file name*).

You also specify whether the categories should appear in the output (default: *no*).

Last but not least, you can scale the buttons if they are too big or too small for you.

### Import and Export

You can import and export the settings to transfer them to other computers.
This saves a *properties* file, a plain text file in which the settings are stored.


## Statistics

The statistics (*Statistik → Daten*) contains various statistics about the data.
It is currently more of a gimmick.


## Help

*Hilfe → Handbuch…* opens this online manual in a help window.

Display short information about the program with *Hilfe → Über…*.
