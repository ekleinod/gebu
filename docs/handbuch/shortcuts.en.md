# Shortcuts

The ESC function: quit the program with ++esc++ :smiley_cat:

## Menu Programm

| Command | Shortcut | Remark |
|--|--|--|
| Anzeige | ++ctrl+shift+e++ | aus der Editoransicht heraus |
| Editor | ++ctrl+e++ | aus der Geburtstagsanzeige heraus |
| Einstellungen | — | |
| Beenden | ++ctrl+q++ | |
| Beenden | ++esc++ | in der Geburtstagsanzeige |

## Menu Datei

| Command | Shortcut |
|--|--|
| Neu | ++ctrl+n++ |
| Öffnen… | ++ctrl+o++ |
| Speichern | ++ctrl+s++ |
| Speichern als… | ++ctrl+shift+s++ |

## Menu Statistik

| Command | Shortcut |
|--|--|
| Daten | ++ctrl+i++ |

## Menu Hilfe

| Command | Shortcut |
|--|--|
| Handbuch… | ++f1++ |
| Über… | ++ctrl+f1++ |
