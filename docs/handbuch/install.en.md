# Installation

You can install Das Gebu-Programm in two ways:

[TOC]

## Installation Program

=== "Linux"

	!!! info
		You need a Java JDK and a JavaFX SDK, install them with the means of your operating system:

		- Java JDK: <https://jdk.java.net>
		- JavaFX SDK: <https://openjfx.io>

	1. download `gebu.deb` from [Sourceforge](https://sourceforge.net/projects/gebu/files/)
	2. open the deb file with your Linux installer (double click or "open with")
	3. start the installation

=== "Windows"

	!!! info
		You need a Java JDK and a JavaFX SDK, these are included in the installer.
		The installer does not need administrator rights, it can be run by any user.

	!!! warning
		The installer is not signed, so you have to click away a Windows warning for the installation.
		If you are unsure or do not trust the installer, do not do this.
		You can also access the jar file directly, as described below.

	1. download `gebu-install_#.#.#.exe` from [Sourceforge](https://sourceforge.net/projects/gebu/files/), where `#.#.#` stands for the desired version
	2. start the exe-file (double click or "Open with")
	3. starting with Windows 10, a warning may appear that Windows protected your PC (unrecognized app), click on "More info" and select "run anyway" (see warning above)
	4. select the language of the installer and the directory in which Das Gebu-Programm should be installed.
	5. select if the Java JDK and/or the JavaFX SDK should also be installed.
		These programs will be installed in the same directory as Das Gebu-Programm.
		If you are unsure if you already have an installation, choose to install them completely, this will take up more disk space but is the safest alternative to having Das Gebu-Programm running.
	6. select the start menu folder name or choose not to create such a folder
	6. select whether you want Das Gebu-Programm to run when the program starts
	7. start the installation


## Direct Call of `gebu.jar`

!!! info
	You need a Java JDK and a JavaFX SDK, install them with the means of your operating system:

	- Java JDK: <https://jdk.java.net>
	- JavaFX SDK: <https://openjfx.io>

=== "Linux"

	1. download `gebu.jar` from [Sourceforge](https://sourceforge.net/projects/gebu/files/)
	2. call Das Gebu-Programm as follows:

		~~~ bash
		<JavaJDK>/bin/java --module-path <JavaFXSDK>/lib/ --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.web -jar gebu.jar
		~~~

	3. put these calls in the start menu or autostart if you like

=== "Windows"

	1. download `gebu.jar` from [Sourceforge](https://sourceforge.net/projects/gebu/files/)
	2. call Das Gebu-Programm as follows:

		~~~ bash
		<JavaJDK>\bin\javaw.exe --module-path <JavaFXSDK>\lib\ --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.web -jar gebu.jar
		~~~

	3. put these calls in the start menu or autostart if you like
