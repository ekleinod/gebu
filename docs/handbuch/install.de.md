# Installation

Sie können Das Gebu-Programm auf zwei Arten installieren:

[TOC]

## Installationsprogramm

=== "Linux"

	!!! info
		Sie benötigen ein Java JDK und ein JavaFX SDK, installieren Sie diese mit den Mitteln Ihres Betriebssystems:

		- Java JDK: <https://jdk.java.net>
		- JavaFX SDK: <https://openjfx.io>

	1. laden Sie `gebu.deb` von [Sourceforge](https://sourceforge.net/projects/gebu/files/) herunter
	2. öffnen Sie die deb-Datei mit ihrem Linux-Installer (Doppelklick oder "Öffnen mit")
	3. starten Sie die Installation

=== "Windows"

	!!! info
		Sie benötigen ein Java JDK und ein JavaFX SDK, diese sind im Installer enthalten.
		Der Installer benötigt keine Administratorrechte, sondern kann von jede:r User:in ausgeführt werden.

	!!! warning "Warnung"
		Der Installer ist nicht signiert, daher müssen Sie für die Installation eine Windows-Warnung wegklicken.
		Wenn Sie unsicher sind oder dem Installer nicht trauen, tun Sie das nicht.
		Sie können die jar-Datei auch direkt aufrufen, wie unten beschrieben.

	1. laden Sie `gebu-install_#.#.#.exe` von [Sourceforge](https://sourceforge.net/projects/gebu/files/) herunter, wobei `#.#.#` für die gewünschte Version steht
	2. starten Sie die exe-Datei (Doppelklick oder "Öffnen mit")
	3. ab Windows 10 erscheint evtl. eine Warnung, dass der Computer durch Windows geschützt wurde (Start einer unbekannten App verhindert), klicken Sie auf "Weitere Informationen" und wählen "trotzdem ausführen" (siehe Warnung oben)
	4. wählen Sie die Sprache des Installers und das Verzeichnis, in dem das Gebu-Programm installiert werden soll
	5. wählen Sie aus, ob das Java JDK und/oder das JavaFX SDK ebenfalls installiert werden sollen.
		Diese Programme werden in das gleiche Verzeichnis wie das Gebu-Programm installiert.
		Wenn Sie unsicher sind, ob Sie bereits eine Installation besitzen, wählen Sie die komplette Installation, dies belegt mehr Speicherplatz, ist jedoch die sicherste Alternative, dass das Gebu-Programm läuft.
	6. wählen Sie den Startmenü-Ordnernamen aus oder lassen Sie keinen Ordner erstellen
	7. wählen Sie aus, ob das Gebu-Programm beim Programmstart ausgeführt werden soll
	8. starten Sie die Installation


## Direkter Aufruf von `gebu.jar`

!!! info
	Sie benötigen ein Java JDK und ein JavaFX SDK, installieren Sie diese mit den Mitteln Ihres Betriebssystems:

	- Java JDK: <https://jdk.java.net>
	- JavaFX SDK: <https://openjfx.io>

=== "Linux"

	1. laden Sie `gebu.jar` von [Sourceforge](https://sourceforge.net/projects/gebu/files/) herunter
	2. rufen Sie Das Gebu-Programm wie folgt auf:

		~~~ bash
		<JavaJDK>/bin/java --module-path <JavaFXSDK>/lib/ --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.web -jar gebu.jar
		~~~

	3. legen Sie diese Aufrufe in das Startmenü oder den Autostart, wenn Sie möchten

=== "Windows"

	1. laden Sie `gebu.jar` von [Sourceforge](https://sourceforge.net/projects/gebu/files/) herunter
	2. rufen Sie Das Gebu-Programm wie folgt auf:

		~~~ bash
		<JavaJDK>\bin\javaw.exe --module-path <JavaFXSDK>\lib\ --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.web -jar gebu.jar
		~~~

	3. legen Sie diese Aufrufe in das Startmenü oder den Autostart, wenn Sie möchten
