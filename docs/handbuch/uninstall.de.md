# Deinstallation

Wenn Sie das Gebu-Programm mit einem Installer installiert haben, können Sie das Programm mit Bordmitteln ihres Betriebssystems deinstallieren.
Unter Windows geschieht das über die Einstellungen, unter Linux über Ihren Paketmanager.

Wenn Sie das Gebu-Programm von Hand installiert haben, löschen Sie die von Ihnen angelegten Dateien.

Falls Sie für das Gebu-Programm extra Java oder JavaFX installiert haben, prüfen Sie, ob diese Programme ebenfalls gelöscht werden sollten.
