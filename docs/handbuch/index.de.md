# Handbuch

![Logo – eine Katze](../images/logo.png){ align=right }

!!! tip ""
	Geburtstage nicht mehr vergessen.

Das Gebu-Programm zeigt Geburtstage in einem Rahmen von zwei Wochen an, d.h. die Geburtstage der vergangenen Woche, die heutigen Geburtstage sowie die zukünftigen Geburtstage der nächsten Woche.

Die Geburtstage können mit einem internen Editor editiert werden.
Das Anzeigeintervall ist einstellbar.

Am einfachsten ist es, das Gebu-Programm mit dem Rechner automatisch zu starten.
So werden die aktuell relevanten Geburtstage und sonstige Ereignisse beim Rechnerstart angezeigt.

Mit der ESC-Funktion (++esc++-Taste) wird das Programm schnell geschlossen.
