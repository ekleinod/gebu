# Bedienung

![Logo – a cat](../images/logo.png){ align=right }

!!! info
	Auf dieser Seite werden nur die Menübefehle angegeben, die aufgerufen werden sollen.
	Die zugehörigen Tastenkürzel werden jeweils im Menü angezeigt oder können auf der [Tastenkürzel-Seite](../shortcuts) nachgeschlagen werden.


## Starten des Gebu-Programms

Nach der [Installation](../install) können Sie das Gebu-Programm aus dem Startmenü starten oder es wird automatisch mit dem Rechner gestartet.

Wenn Sie das Programm nicht installiert oder eine Verknüpfung erstellt haben, starten Sie das Gebu-Programm durch direkten Aufruf, wie in [Installation – direkter Aufruf](../install/#direkter-aufruf-von-gebujar) beschrieben.

Beim Starten wird automatisch die zuletzt geöffnete Datei geladen und die Geburtstagsanzeige angezeigt.
Wurde noch keine Datei geöffnet, startet das Programm mit der Editor-Ansicht.


## Geburtstagsanzeige

Die Geburtstagsanzeige zeigt die im Anzeigeintervall liegenden Geburtstage an.
Sie wird beim Start angezeigt, wenn eine Geburtstagsdatei geladen wurde.

Die Anzeige kann in den Einstellungen (*Programm → Einstellungen*) angepasst werden.

Mit *Programm → Editor* wechseln Sie in den Editiermodus (Bearbeitungsmodus).


## Bearbeitung

Im Editiermodus (Bearbeitungsmodus) können Sie Daten eidieren, neue Dateien anlegen oder eine Statistik aufrufen.

Mit *Programm → Anzeige* wechseln Sie zur Geburtstagsanzeige.

### Neue Datei anlegen

Legen Sie eine neue Datei über *Datei → Neu* an.

Wenn Sie ungespeicherte Änderungen haben, speichern Sie diese vor dem Anlegen einer neuen Datei.

### Datei öffnen

Öffnen Sie eine bestehende Datei über *Datei → Öffnen…*.
Die zuletzt geöffnete Datei wird beim Programmstart automatisch wieder geöffnet.

Wenn Sie ungespeicherte Änderungen haben, speichern Sie diese vor dem Öffnen einer Datei.

### Ereignisse anzeigen, eingeben, ändern und löschen

Im Editiermodus (Bearbeitungsmodus) wird links eine Liste mit allen vorhandenen Daten angezeigt.
Rechts davon ist die Detailanzeige, die zu den links ausgewählten Ereignissen die Details anzeigt.

Am unteren Rand der Detailanzeige sind Schaltflächen für Anlegen, Bearbeiten und Löschen von Ereignissen: ++"Neu…"++, ++"Ändern…"++ und ++"Löschen"++.
Sie können Ereignisse auch durch Doppelklick in der Liste zum Bearbeiten öffnen.

Wenn Sie neue Ereignisse anlegen oder vorhandene bearbeiten, öffnet sich der Ereigniseditor.
Hier können Sie Datum, Titel, Ereignisart und Kategorie eingeben.

Datum, Titel und Ereignisart sind Pflichtfelder.

Ereignisart und Kategorie zeigen vorhandene Einträge an, die ausgewählt werden können.
Wenn Sie eine neue Ereignisart oder Kategorie anlegen wollen, geben Sie diese einfach in das Feld ein.

Mit ++"OK"++ übernehmen Sie die Änderungen.
Mit ++"Abbrechen"++ verwerfen Sie die Änderungen.

Wenn Sie Ereignisse löschen, erscheint eine entsprechende Nachfrage, die mit Klick auf ++"OK"++ das Ereignis löscht.

Nachdem Sie Daten eingegeben, geändert oder gelöscht haben, müssen Sie die Datei speichern.
Das Programm speichert Dateien nicht automatisch, weist aber auf nicht gespeicherte Änderungen hin.

### Daten speichern

Die eingegeben Daten werden lokal auf dem Rechner in einer Datei gespeichert.
Dafür muss beim ersten Speichern ein Dateiname vergeben werden.

Die Daten werden nicht verschlüsselt.


## Einstellungen

In den Einstellungen (*Programm → Einstellungen*) können Sie das Verhalten bzw. Aussehen des Programms beeinflussen.

### Ausblendungen

Sie können Ereignisarten oder Kategorien von der Anzeige ausschließen.
Dafür müssen Sie die auszublendenden Ereignisarten oder Kategorien hier ankreuzen.

Das ist z.B. hilfreich, wenn die selbe Datei auf unterschiedlichen Rechnern genutzt wird und dort andere Ereignisse angezeigt werden sollen.
Konkret können z.B. private Geburtstage auf Arbeit ausgeblendet werden.

### Ereignisanzeige

Neben dem Anzeigeintervall können Sie Farbe und Schriftgröße für vergangene, aktuelle und zukünftige Ereignisse festlegen.

### Sonstiges

Hier stellen Sie ein, ob im Programmtitel nur der Dateiname oder der gesamte Pfad angezeigt wird (Standard: *nur Dateiname*).

Außerdem legen Sie fest, ob die Kategorien in der Ausgabe erscheinen sollen (Standard: *nein*).

Zu guter Letzt können Sie die Schaltflächen skalieren, falls sie Ihnen zu groß oder zu klein sind.

### Im- und Export

Sie können die Einstellungen im- und exportieren, um sie auf andere Rechner zu übertragen.
Dabei wird eine *properties*-Datei gespeichert, eine Klartextdatei, in der die Einstellungen hinterlegt sind.


## Statistik

Die Statistik (*Statistik → Daten*) enthält verschiedene Statistiken über die Daten.
Sie ist derzeit eher eine Spielerei.


## Hilfe

*Hilfe → Handbuch…* öffnet dieses Online-Handbuch in einem Hilfefenster.

Mit *Hilfe → Über…* werden kurze Informationen über das Programm angezeigt.
