# Tastenkürzel

Die ESC-Funktion: Beenden des Programms mit ++esc++ :smiley_cat:

## Programm-Menü

| Befehl | Tastenkürzel | Anmerkung |
|--|--|--|
| Anzeige | ++ctrl+shift+e++ | aus der Editoransicht heraus |
| Editor | ++ctrl+e++ | aus der Geburtstagsanzeige heraus |
| Einstellungen | — | |
| Beenden | ++ctrl+q++ | |
| Beenden | ++esc++ | in der Geburtstagsanzeige |

## Datei-Menü

| Befehl | Tastenkürzel |
|--|--|
| Neu | ++ctrl+n++ |
| Öffnen… | ++ctrl+o++ |
| Speichern | ++ctrl+s++ |
| Speichern als… | ++ctrl+shift+s++ |

## Statistik-Menü

| Befehl | Tastenkürzel |
|--|--|
| Daten | ++ctrl+i++ |

## Hilfe-Menü

| Befehl | Tastenkürzel |
|--|--|
| Handbuch… | ++f1++ |
| Über… | ++ctrl+f1++ |
