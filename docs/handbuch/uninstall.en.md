# Uninstall

If you have installed Das Gebu-Programm with an installer, you can uninstall the program using the on-board tools of your operating system.
Under Windows this is done via the settings, under Linux via your package manager.

If you installed Das Gebu-Programm manually, delete the files you created.

If you have installed Java or JavaFX for Das Gebu-Programm, check whether these programs should also be deleted.
