# Participate

!!! question ""
	What is this and how can I participate?

*Das Gebu-Programm* is a free open source program.

That means you can use the program for free, see the source code and also improve the program if you want.

If you want to participate but can't program, you can report bugs, make suggestions for improvement, contribute translations or improve the manual.

Bugs and feature requests are collected on the internet as *issue* at *gitlab*.
The code as well as the manual can also be found at *gitlab*.
You can submit changes as *issue* or as *merge request*:

Source code
: <https://gitlab.com/ekleinod/gebu>

Manual
: <https://ekleinod.gitlab.io/gebu/>

Report bugs
: <https://gitlab.com/ekleinod/gebu/-/issues>

Report requests
: <https://gitlab.com/ekleinod/gebu/-/issues>

Translations
: I'll announce when it's ready, the technical basis for it is not implemented yet
