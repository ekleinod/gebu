# Legal

![Logo – a cat](images/logo.png){ align=right }


*Das Gebu-Programm* is a free open source program.


## Resources

The following resources were used:

- Logo
	- [Happy-White-Cat](https://freesvg.org/happy-white-cat) by [FreeSVG](https://freesvg.org/)
	- [Master of Break](https://www.dafont.com/de/font-comment.php?file=master_of_break) by [DaFont](https://www.dafont.com/)
- Icons
	- [Papirus Icon Theme](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme-gtk)
	- [FreeSVG](https://freesvg.org)
- Programming
	- [eclipse](https://www.eclipse.org)
	- [Java](https://docs.oracle.com/en/java/javase/)
	- [JavaFX](https://openjfx.io/)
	- [gitlab](https://gitlab.com/ekleinod/gebu/)
- Documentation
	- [MkDocs](https://www.mkdocs.org)
	- [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)
- CakePops
	- [futtermeister](https://twitter.com/futtermeister)

## License

*The Gebu program* is licensed under the [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html).

The license can be read [here](https://www.gnu.org/licenses/gpl-3.0.en.html) in the original.

In summary, the license means that this program can be distributed and used freely.
The source code is available and can be adapted as desired.
However, if an adaptation of the program is published, this may only be done under the GPL as well.
