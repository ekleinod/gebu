# Rechtliches

![Logo – eine Katze](images/logo.png){ align=right }


*Das Gebu-Programm* ist ein kostenloses Open-Source-Programm.


## Ressourcen

Folgende Ressourcen wurden benutzt:

- Logo
	- [Happy-White-Cat](https://freesvg.org/happy-white-cat) von [FreeSVG](https://freesvg.org/)
	- [Master of Break](https://www.dafont.com/de/font-comment.php?file=master_of_break) von [DaFont](https://www.dafont.com/)
- Icons
	- [Papirus Icon Theme](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme-gtk)
	- [FreeSVG](https://freesvg.org)
- Programmierung
	- [eclipse](https://www.eclipse.org)
	- [Java](https://docs.oracle.com/en/java/javase/)
	- [JavaFX](https://openjfx.io/)
	- [gitlab](https://gitlab.com/ekleinod/gebu/)
- Dokumentation
	- [MkDocs](https://www.mkdocs.org)
	- [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)
- CakePops
	- [futtermeister](https://twitter.com/futtermeister)

## Lizenz

*Das Gebu-Programm* steht unter der [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html).

Die Lizenz ist [hier](https://www.gnu.org/licenses/gpl-3.0.en.html) im Original nachlesbar.

Zusammengefasst bedeutet die Lizenz, dass dieses Program frei verteilt und benutzt werden kann.
Der Quellcode steht zur Verfügung und kann beliebig angepasst werden.
Wenn jedoch eine Anpassung des Programms veröffentlicht wird, darf das nur ebenfalls unter der GPL geschehen.
