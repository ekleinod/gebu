# Introduction

![Logo – a cat](../images/logo.png){ align=right }

!!! tip ""
	Development takes place in English.

These pages document some of the development issues, they are not needed in order to use the program.

## Programs needed

This is a list of programs needed for development, it is not complete but should be sufficient for most tasks

- Java
- JavaFX
- eclipse
- ant
- maven
- docker
- dpkg
