# A New Release

![Logo – a cat](../images/logo.png){ align=right }

!!! info
	Feel free to commit as often as you wish in case one script or change does not work as expected.

In order to publish a new release follow these steps.

## Preparation

1. finish programming
2. merge all changes to `main`
3. create a release feature branch `feature/release-#.#.#`

## Updates

1. update readme
2. update both changelogs (one for humans, the other for debian :smile:)
3. update version in `pom.xml` (check for other updates as well)
4. update version and copyright in `gebu/src/main/resources/de/edgesoft/gebu/resources/project.properties`, and `build/win/gebu.iss`
5. update copyright in `build/build.xml`, and `build/win/build.xml`
6. update copyright in all other files per script

	~~~ shell
	$ cd build
	$ ant setcopyright
	~~~
7. update documentation, and manual

## Create jar

1. run `maven clean compile test` (run configuration `gebu clean compile test`)
2. run `maven clean compile package` (run configuration `gebu clean compile package`)
3. rename generated jar `gebu/target/gebu-#.#.#-jar-with-dependencies.jar` to `gebu/target/gebu.jar`
4. test if the jar can be started and works (should work on the development computer, if Java and JavaFX are installed and set correctly)

	~~~ shell
	$ cd gebu/target
	$ java -jar gebu.jar
	~~~

5. upload `README.md` to [sourceforge](https://sourceforge.net/projects/gebu/files/)
6. upload `gebu.jar` to [sourceforge](https://sourceforge.net/projects/gebu/files/)


## Debian installer

1. update recommendations in `build/debian/templates/DEBIAN/control`
2. create debian installer (`gebu.deb`)

	~~~ shell
	$ cd build/debian
	$ sudo ant preparedeb
	$ ant createdeb
	$ sudo ant cleardeb
	~~~

3. test debian installer
4. upload debian installer to [sourceforge](https://sourceforge.net/projects/gebu/files/)


## Windows installer

1. download zips of [Java JDK](https://jdk.java.net) and [JavaFX SDK](https://openjfx.io) for Windows to `build/win/install-files`, unpack the archives
2. rename the directories `Java-JDK-#.#.#` and `JavaFX-SDK-#.#.#` respectively, where `#.#.#` stands for the version number of the JDK and SDK
3. update versions and copyright in `build/win/gebu.iss`, *define* section
4. create windows installer (`gebu-install_#.#.#.exe`)

	~~~ shell
	$ cd build/win
	$ ant innosetup
	~~~

5. test windows installer
6. upload windows installer to [sourceforge](https://sourceforge.net/projects/gebu/files/)

## Create source code release

1. check if documentation can be generated without errors, stop server with ++ctrl+c++

	~~~ shell
	$ ./serve-docs.sh
	~~~

2. commit all changes
3. merge release feature branch into `main`
4. push to gitlab in order to check documentation CI/CD
5. create release branch (major and minor changes) from main or use existing release branch (bugfix/patch) `release/#.#`
	1. when reusing release branch, merge `main` into release branch
6. create annotated tag `#.#.#`
7. checkout `main` to avoid using release branches for development
8. push all branches and tags to gitlab
9. in gitlab, create release from tag, include links to sourceforge installer downloads

## Enjoy

:smile:
