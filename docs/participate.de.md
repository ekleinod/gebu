# Mitmachen

!!! question ""
	Was soll das und wie kann ich mitmachen?

*Das Gebu-Programm* ist ein kostenloses Open-Source-Programm.

Das heißt, Sie können das Programm kostenlos benutzen, den Quellcode einsehen und das Programm auch verbessern, wenn Sie wollen.

Wenn Sie sich beteiligen wollen, aber nicht programmieren können, so können Sie Fehler melden, Verbesserungsvorschläge machen, Übersetzungen beitragen oder das Handbuch verbessern.

Fehler und Wünsche werden im Internet als *Issue* bei *gitlab* gesammelt.
Der Code sowie das Handbuch sind ebenfalls in *gitlab* zu finden.
Sie können Änderungen als *Issue* einreichen oder als *Merge Request*:

Quellcode
: <https://gitlab.com/ekleinod/gebu>

Handbuch
: <https://ekleinod.gitlab.io/gebu/>

Fehler melden
: <https://gitlab.com/ekleinod/gebu/-/issues>

Wünsche melden
: <https://gitlab.com/ekleinod/gebu/-/issues>

Übersetzungen
: gebe ich bekannt, wenn es soweit ist, derzeit ist die technische Basis dafür noch nicht da
