package de.edgesoft.gebu.jaxb_legacy_5_2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.edgesoft.edgeutils.jaxb.JAXBFiles;

/**
 * Unit test for Gebu.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
public class GebuLegacyTest {

	/** File name. */
	private static final String FILENAME = String.format("src/test/resources/%s.%s.xml", GebuLegacyTest.class.getPackage().getName(), GebuLegacyTest.class.getSimpleName().toLowerCase());

	/**
	 * Delete files.
	 * @throws Exception
	 */
	@BeforeEach
	public void deleteFiles() {
		try {
			Files.deleteIfExists(Paths.get(FILENAME));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Tests marshalling.
	 */
	@Test
	public void testMarshalFile() throws Exception {
		
		ObjectFactory factory = new ObjectFactory();

		Gebu gebu = factory.createGebu();
		
		Program program = factory.createProgram();
		program.setName(GebuLegacyTest.class.getCanonicalName());
		program.setVersion("6.0.0");
		
		gebu.setProgram(program);
		
		Data data = factory.createData();
		gebu.setData(data);
		
		Event event = factory.createEvent();
		event.setDescription("Johann Wolfgang von Goethe");
		event.setEventname("Geburtstag");
		event.setDate(LocalDate.of(1749, 8, 28));
		event.setCategory("Dichter");
		data.getEvent().add(event);
		
		event = factory.createEvent();
		event.setDescription("Johann Wolfgang von Goethe");
		event.setEventname("Todestag");
		event.setDate(LocalDate.of(1832, 2, 22));
		event.setCategory("Dichter");
		data.getEvent().add(event);
		
		event = factory.createEvent();
		event.setDescription("Friedrich Schiller");
		event.setEventname("Geburtstag");
		event.setDate(LocalDate.of(1759, 11, 10));
		event.setCategory("Dichter");
		data.getEvent().add(event);
		
		event = factory.createEvent();
		event.setDescription("Charlotte von Lengefeld");
		event.setEventname("Geburtstag");
		event.setDate(LocalDate.of(1766, 11, 22));
		data.getEvent().add(event);
		
		event = factory.createEvent();
		event.setDescription("Die Schillers");
		event.setEventname("Hochzeitstag");
		event.setDate(LocalDate.of(1790, 2, 22));
		event.setCategory("Dichterehen");
		data.getEvent().add(event);
		
		JAXBFiles.marshal(factory.createGebu(gebu), FILENAME, null);


		
		Gebu readGebu = JAXBFiles.unmarshal(FILENAME, Gebu.class);
		
		assertEquals(5, readGebu.getData().getEvent().size());
		assertEquals(LocalDate.of(1749, 8, 28), readGebu.getData().getEvent().stream().findFirst().get().getDate());

	}

}

/* EOF */
