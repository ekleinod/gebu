package de.edgesoft.gebu.jaxb;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import de.edgesoft.edgeutils.datetime.DateTimeUtils;
import de.edgesoft.gebu.model.ContentModel;
import de.edgesoft.gebu.model.EventModel;
import de.edgesoft.gebu.utils.LocalPrefs;
import de.edgesoft.gebu.utils.TimeCategories;
import javafx.util.Pair;

/**
 * Unit test for filtering gebu data.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
public class GebuFilterTest {

	/**
	 * Gebu data.
	 */
	private static Gebu testData = null;

	/**
	 * Create test data.
	 */
	@BeforeAll
	public static void createTestData() {
		LocalPrefs.init(de.edgesoft.gebu.Gebu.class);
		testData = GebuTestData.getTestData();

		System.out.println("unfiltered");
		testData.getContent().getEvent().stream()
				.sorted(EventModel.DATE_TITLE)
				.map(Event::getDate)
				.forEach(System.out::println);
		System.out.println();

	}


	/**
	 * Definition of test arguments.
	 *
	 * @return stream of arguments
	 */
	private static Stream<Arguments> createArguments() {

		return Stream.of(
				Arguments.of(
						"2015-03-01",
						Map.of(
								TimeCategories.PAST, new Pair<>(3, "1790-02-22"),
								TimeCategories.PRESENT, new Pair<>(2, "1995-03-01"),
								TimeCategories.FUTURE, new Pair<>(0, null)
								)
						),
				Arguments.of(
						"2016-03-01",
						Map.of(
								TimeCategories.PAST, new Pair<>(2, "1996-02-28"),
								TimeCategories.PRESENT, new Pair<>(2, "1995-03-01"),
								TimeCategories.FUTURE, new Pair<>(0, null)
								)
						),
				Arguments.of(
						"2016-01-03",
						Map.of(
								TimeCategories.PAST, new Pair<>(6, "1996-12-27"),
								TimeCategories.PRESENT, new Pair<>(1, "1995-01-03"),
								TimeCategories.FUTURE, new Pair<>(1, "1992-01-04")
								)
						),
				Arguments.of(
						"2015-01-03",
						Map.of(
								TimeCategories.PAST, new Pair<>(6, "1996-12-27"),
								TimeCategories.PRESENT, new Pair<>(1, "1995-01-03"),
								TimeCategories.FUTURE, new Pair<>(1, "1992-01-04")
								)
						),
				Arguments.of(
						"2016-01-01",
						Map.of(
								TimeCategories.PAST, new Pair<>(7, "1996-12-26"),
								TimeCategories.PRESENT, new Pair<>(1, "1995-01-01"),
								TimeCategories.FUTURE, new Pair<>(2, "1995-01-03")
								)
						),
				Arguments.of(
						"2015-01-01",
						Map.of(
								TimeCategories.PAST, new Pair<>(7, "1996-12-26"),
								TimeCategories.PRESENT, new Pair<>(1, "1995-01-01"),
								TimeCategories.FUTURE, new Pair<>(2, "1995-01-03")
								)
						),
				Arguments.of(
						"2016-01-08",
						Map.of(
								TimeCategories.PAST, new Pair<>(3, "1995-01-01"),
								TimeCategories.PRESENT, new Pair<>(0, null),
								TimeCategories.FUTURE, new Pair<>(0, null)
								)
						),
				Arguments.of(
						"2015-01-08",
						Map.of(
								TimeCategories.PAST, new Pair<>(3, "1995-01-01"),
								TimeCategories.PRESENT, new Pair<>(0, null),
								TimeCategories.FUTURE, new Pair<>(0, null)
								)
						),
				Arguments.of(
						"2015-12-24",
						Map.of(
								TimeCategories.PAST, new Pair<>(0, null),
								TimeCategories.PRESENT, new Pair<>(2, "1992-12-24"),
								TimeCategories.FUTURE, new Pair<>(7, "1996-12-26")
								)
						),
				Arguments.of(
						"2016-12-24",
						Map.of(
								TimeCategories.PAST, new Pair<>(0, null),
								TimeCategories.PRESENT, new Pair<>(2, "1992-12-24"),
								TimeCategories.FUTURE, new Pair<>(7, "1996-12-26")
								)
						),
				Arguments.of(
						"2015-12-27",
						Map.of(
								TimeCategories.PAST, new Pair<>(4, "1992-12-24"),
								TimeCategories.PRESENT, new Pair<>(2, "1996-12-27"),
								TimeCategories.FUTURE, new Pair<>(5, "1990-12-28")
								)
						),
				Arguments.of(
						"2016-12-27",
						Map.of(
								TimeCategories.PAST, new Pair<>(4, "1992-12-24"),
								TimeCategories.PRESENT, new Pair<>(2, "1996-12-27"),
								TimeCategories.FUTURE, new Pair<>(5, "1990-12-28")
								)
						),
				Arguments.of(
						"1972-08-29",
						Map.of(
								TimeCategories.PAST, new Pair<>(1, "1749-08-28"),
								TimeCategories.PRESENT, new Pair<>(0, null),
								TimeCategories.FUTURE, new Pair<>(0, null)
								)
						),
				Arguments.of(
						"1971-08-29",
						Map.of(
								TimeCategories.PAST, new Pair<>(1, "1749-08-28"),
								TimeCategories.PRESENT, new Pair<>(0, null),
								TimeCategories.FUTURE, new Pair<>(0, null)
								)
						),
				Arguments.of(
						"1982-02-01",
						Map.of(
								TimeCategories.PAST, new Pair<>(0, null),
								TimeCategories.PRESENT, new Pair<>(0, null),
								TimeCategories.FUTURE, new Pair<>(0, null)
								)
						),
				Arguments.of(
						"1980-02-01",
						Map.of(
								TimeCategories.PAST, new Pair<>(0, null),
								TimeCategories.PRESENT, new Pair<>(0, null),
								TimeCategories.FUTURE, new Pair<>(0, null)
								)
						)
				);

	}

	/**
	 * Tests filtering.
	 *
	 * @param theDate date to test
	 * @param theResults expected results
	 */
	@ParameterizedTest(name = "{index} ==> {0}")
	@MethodSource("createArguments")
	public void testFiltering(final String theDate, final Map<TimeCategories, Pair<Integer, String>> theResults) {
		
		LocalDate dteTest = DateTimeUtils.parseDate(theDate, "yyyy-MM-dd");

		filterAndCheckResult("previous 7 days not including {0}", -7, -1, dteTest, theResults.get(TimeCategories.PAST));
		filterAndCheckResult("exactly {0}", 0, 0, dteTest, theResults.get(TimeCategories.PRESENT));
		filterAndCheckResult("next 7 days not including {0}", 1, 7, dteTest, theResults.get(TimeCategories.FUTURE));

	}

	/**
	 * Check result of filtering
	 *
	 * @param theExpectedResult expected result
	 * @param theEvents filtered event list
	 */
	private static void filterAndCheckResult(
			final String theTitle,
			final int theLowerBorder,
			final int theUpperBorder,
			final LocalDate theDate,
			final Pair<Integer, String> theExpectedResult
			) {

		System.out.println(MessageFormat.format(theTitle, DateTimeUtils.formatDate(theDate)));
		List<Event> lstFiltered = ((ContentModel) testData.getContent()).getSortedFilterEvents(theDate, theLowerBorder, theUpperBorder);
		lstFiltered.stream()
				.map(Event::getDate)
				.forEach(System.out::println);
		System.out.println();

		assertEquals(theExpectedResult.getKey(), lstFiltered.size());
		if (theExpectedResult.getValue() != null) {
			assertEquals(DateTimeUtils.parseDate(theExpectedResult.getValue(), "yyyy-MM-dd"), lstFiltered.get(0).getDate().getValue());
		}

	}

}

/* EOF */
