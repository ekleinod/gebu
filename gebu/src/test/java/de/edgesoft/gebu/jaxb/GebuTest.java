package de.edgesoft.gebu.jaxb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import de.edgesoft.edgeutils.jaxb.JAXBFiles;
import de.edgesoft.gebu.model.EventModel;

/**
 * Unit test for Gebu.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
public class GebuTest {

	/** 
	 * File name. 
	 */
	private static final String FILENAME = String.format("src/test/resources/%s.%s.xml", GebuTest.class.getPackage().getName(), GebuTest.class.getSimpleName().toLowerCase());

	/** 
	 * Gebu data. 
	 */
	private static Gebu testData = null;

	/**
	 * Delete files.
	 */
	@BeforeAll
	public static void deleteFiles() {
		try {
			Files.deleteIfExists(Paths.get(FILENAME));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Create test data.
	 */
	@BeforeAll
	public static void createTestData() {
		testData = GebuTestData.getTestData();
	}

	/**
	 * Tests marshalling and unmarshalling.
	 */
	@Test
	public void testMarshalFile() throws Exception {
		
		JAXBFiles.marshal(new ObjectFactory().createGebu(testData), FILENAME, null);

		Gebu readGebu = JAXBFiles.unmarshal(FILENAME, Gebu.class);

		assertEquals(21, readGebu.getContent().getEvent().size());
		assertEquals(LocalDate.of(1749, 8, 28), readGebu.getContent().getEvent().stream().findFirst().get().getDate().getValue());

	}

	/**
	 * Tests sorting.
	 */
	@Test
	public void testSorting() {
		
		// unsorted
		System.out.println("unsorted");
		System.out.println(testData);
		System.out.println(testData.getContent());
		System.out.println(testData.getContent().getEvent());
		testData.getContent().getEvent().stream()
				.map(Event::getDate)
				.forEach(System.out::println);
		System.out.println();

		// sorted
		System.out.println("sorted");
		testData.getContent().getEvent().stream()
				.sorted(EventModel.DATE_TITLE)
				.map(Event::getDate)
				.forEach(System.out::println);
		System.out.println();

		// first element
		assertEquals(LocalDate.of(1995, 1, 1), testData.getContent().getEvent().stream().sorted(EventModel.DATE_TITLE).findFirst().get().getDate().getValue());
		
		// last element
		List<Event> lstEvents = testData.getContent().getEvent().stream().sorted(EventModel.DATE_TITLE).collect(Collectors.toList());
		assertEquals(LocalDate.of(1996, 12, 31), lstEvents.get(lstEvents.size() - 1).getDate().getValue());

	}

}

/* EOF */
