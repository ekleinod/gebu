package de.edgesoft.gebu.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import de.edgesoft.edgeutils.datetime.DateTimeUtils;
import de.edgesoft.gebu.jaxb.ObjectFactory;
import javafx.beans.property.SimpleObjectProperty;

/**
 * Unit test for events.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
public class EventTest {

	/**
	 * Definition of test arguments.
	 *
	 * @return stream of arguments
	 */
	private static Stream<Arguments> createArgumentsGetDisplayAge() {

		return Stream.of(
				
				Arguments.of("1996-01-01", "1999-12-01", 4),
				Arguments.of("1996-01-01", "2000-01-01", 4),
				Arguments.of("1996-01-01", "2000-02-01", 4),
				Arguments.of("1996-01-01", "2000-05-01", 4),
				Arguments.of("1996-01-01", "2000-11-01", 4),
				Arguments.of("1996-01-01", "2000-12-01", 5),
				Arguments.of("1996-01-01", "2001-01-01", 5),
				
				Arguments.of("1996-12-24", "1999-12-01", 3),
				Arguments.of("1996-12-24", "2000-01-01", 3),
				Arguments.of("1996-12-24", "2000-02-01", 4),
				Arguments.of("1996-12-24", "2000-05-01", 4),
				Arguments.of("1996-12-24", "2000-11-01", 4),
				Arguments.of("1996-12-24", "2000-12-01", 4),
				Arguments.of("1996-12-24", "2001-01-01", 4)
				
				);
		
	}
	
	/**
	 * Tests method {@link EventModel#getDisplayAge(LocalDate)}.
	 *
	 * @param theBirthday birthday
	 * @param theDate date to compare to
	 * @param theAge expected age
	 */
	@ParameterizedTest(name = "{index} ==> {0}")
	@MethodSource("createArgumentsGetDisplayAge")
	public void testGetDisplayAge(final String theBirthday, final String theDate, final Integer theAge) {
		
		EventModel evtTest = (EventModel) new ObjectFactory().createEvent();
		evtTest.setDate(new SimpleObjectProperty<>(DateTimeUtils.parseDate(theBirthday, "yyyy-MM-dd")));
		
		assertEquals(theAge, evtTest.getDisplayAge(DateTimeUtils.parseDate(theDate, "yyyy-MM-dd")));
		
	}

}

/* EOF */
