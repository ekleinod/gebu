/**
 * Module info file.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
module de.edgesoft.gebu {

	// module exports
	exports de.edgesoft.gebu;
	exports de.edgesoft.gebu.controller to javafx.fxml;

	// opens to edgeutils and other helper classes in order to load views and resources
	opens de.edgesoft.gebu.view to de.edgesoft.edgeutils;
	opens de.edgesoft.gebu.resources to de.edgesoft.edgeutils;
	opens de.edgesoft.gebu.resources.css to de.edgesoft.edgeutils;
	opens de.edgesoft.gebu.resources.images to de.edgesoft.edgeutils;
	opens de.edgesoft.gebu.resources.icons.actions to de.edgesoft.edgeutils;
	opens de.edgesoft.gebu.resources.icons.status to de.edgesoft.edgeutils;
	
	// opens to freemarker as well for template loading
	opens de.edgesoft.gebu.resources.templates to de.edgesoft.edgeutils, freemarker;
	
	// opens to fxml for controller loading from fxml file
	opens de.edgesoft.gebu.controller to javafx.fxml;

	// opens to xml.bind and freemarker for using jaxb classes
	opens de.edgesoft.gebu.jaxb to java.xml.bind, freemarker;
	opens de.edgesoft.gebu.jaxb_legacy_5_2 to java.xml.bind, freemarker;
	
	// opens to all, because it is needed by a unnamed module
	// TODO find out, why this is needed
	opens de.edgesoft.gebu.model;
	

	// simple requirements
	requires java.sql;
	requires java.xml.bind;
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;
	requires javafx.web;
	
	// transitive requirements
	requires transitive de.edgesoft.edgeutils;

	// transitive requirements with unstable names - no module info in the package
	requires transitive freemarker;
	
}

/* EOF */
