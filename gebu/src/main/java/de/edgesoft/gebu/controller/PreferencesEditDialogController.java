package de.edgesoft.gebu.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.Optional;
import java.util.prefs.BackingStoreException;
import java.util.prefs.InvalidPreferencesFormatException;

import de.edgesoft.edgeutils.files.Prefs;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.javafx.ColorUtils;
import de.edgesoft.gebu.model.AppModel;
import de.edgesoft.gebu.model.ContentModel;
import de.edgesoft.gebu.utils.AlertUtils;
import de.edgesoft.gebu.utils.LocalPrefs;
import de.edgesoft.gebu.utils.PrefKey;
import javafx.fxml.FXML;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.Tab;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Controller for preferences edit dialog scene.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
public class PreferencesEditDialogController {
	
	/**
	 * Preferences string categories.
	 */
	private final String PREF_CATEGORY = "disable.category.%s";

	/**
	 * Preferences string event types.
	 */
	private final String PREF_EVENTTYPE = "disable.eventtype.%s";

	/**
	 * Interval spinner.
	 */
	@FXML
	private Spinner<Integer> spnInterval;

	/**
	 * Fontsize spinner past.
	 */
	@FXML
	private Spinner<Integer> spnPastFontsize;

	/**
	 * Foreground color picker past.
	 */
	@FXML
	private ColorPicker pckPastForeground;

	/**
	 * Backgroud color picker past.
	 */
	@FXML
	private ColorPicker pckPastBackground;

	/**
	 * Fontsize spinner present.
	 */
	@FXML
	private Spinner<Integer> spnPresentFontsize;

	/**
	 * Foreground color picker present.
	 */
	@FXML
	private ColorPicker pckPresentForeground;

	/**
	 * Backgroud color picker present.
	 */
	@FXML
	private ColorPicker pckPresentBackground;

	/**
	 * Fontsize spinner future.
	 */
	@FXML
	private Spinner<Integer> spnFutureFontsize;

	/**
	 * Foreground color picker future.
	 */
	@FXML
	private ColorPicker pckFutureForeground;

	/**
	 * Backgroud color picker future.
	 */
	@FXML
	private ColorPicker pckFutureBackground;

	/**
	 * Checkbox: full path in title.
	 */
	@FXML
	private CheckBox chkTitleFullpath;

	/**
	 * Checkbox: display categories.
	 */
	@FXML
	private CheckBox chkDisplayCategories;

	/**
	 * VBox for categories.
	 */
	@FXML
	private VBox boxCategories;

	/**
	 * VBox for eventtypes.
	 */
	@FXML
	private VBox boxEventtypes;

	/**
	 * Button size.
	 */
	@FXML
	private Slider sldButtonSize;

	/**
	 * OK button.
	 */
	@FXML
	private Button btnOK;

	/**
	 * Cancel button.
	 */
	@FXML
	private Button btnCancel;

	/**
	 * Import button.
	 */
	@FXML
	private Button btnImport;

	/**
	 * Export button.
	 */
	@FXML
	private Button btnExport;

	/**
	 * Tab disable events.
	 */
	@FXML
	private Tab tabDisable;

	/**
	 * Tab display.
	 */
	@FXML
	private Tab tabDisplay;

	/**
	 * Tab others.
	 */
	@FXML
	private Tab tabOthers;

	/**
	 * Reference to dialog stage.
	 */
	private Stage dialogStage;

	/**
	 * OK clicked?.
	 */
	private boolean okClicked;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

		// icons
		btnOK.setGraphic(new ImageView(Resources.loadImage("icons/actions/dialog-ok-16.svg")));
		btnCancel.setGraphic(new ImageView(Resources.loadImage("icons/actions/dialog-cancel-16.svg")));
		
		tabDisable.setGraphic(new ImageView(Resources.loadImage("icons/actions/action-unavailable.svg")));
		tabDisplay.setGraphic(new ImageView(Resources.loadImage("icons/actions/view-calendar-birthday.svg")));
		tabOthers.setGraphic(new ImageView(Resources.loadImage("icons/actions/view-list-details.svg")));
		
		// fill values
		fillValues();
		
    }

	/**
	 * Sets dialog stage.
	 *
	 * @param theStage dialog stage
	 */
	public void setDialogStage(final Stage theStage) {
        dialogStage = theStage;
    }

	/**
	 * Fill preference values.
	 */
	private void fillValues() {
		
		// tab interval/color
		spnInterval.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 365, LocalPrefs.getInt(PrefKey.INTERVAL)));

		spnPastFontsize.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(6, 20, LocalPrefs.getInt(PrefKey.PAST_FONTSIZE)));
		pckPastForeground.setValue(Color.web(LocalPrefs.get(PrefKey.PAST_FOREGROUND)));
		pckPastBackground.setValue(Color.web(LocalPrefs.get(PrefKey.PAST_BACKGROUND)));

		spnPresentFontsize.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(6, 20, LocalPrefs.getInt(PrefKey.PRESENT_FONTSIZE)));
		pckPresentForeground.setValue(Color.web(LocalPrefs.get(PrefKey.PRESENT_FOREGROUND)));
		pckPresentBackground.setValue(Color.web(LocalPrefs.get(PrefKey.PRESENT_BACKGROUND)));

		spnFutureFontsize.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(6, 20, LocalPrefs.getInt(PrefKey.FUTURE_FONTSIZE)));
		pckFutureForeground.setValue(Color.web(LocalPrefs.get(PrefKey.FUTURE_FOREGROUND)));
		pckFutureBackground.setValue(Color.web(LocalPrefs.get(PrefKey.FUTURE_BACKGROUND)));

		// tab disable
        ((ContentModel) AppModel.getData().getContent()).getCategories().stream()
				.forEach(category -> {
					CheckBox chkTemp = new CheckBox(category);
					chkTemp.setSelected(Boolean.parseBoolean(LocalPrefs.get(String.format(PREF_CATEGORY, category))));
					boxCategories.getChildren().add(chkTemp);
				});
        ((ContentModel) AppModel.getData().getContent()).getEventtypes().stream()
				.forEach(eventtype -> {
					CheckBox chkTemp = new CheckBox(eventtype);
					chkTemp.setSelected(Boolean.parseBoolean(LocalPrefs.get(String.format(PREF_EVENTTYPE, eventtype))));
					boxEventtypes.getChildren().add(chkTemp);
				});
        
        // tab display
        chkTitleFullpath.setSelected(LocalPrefs.getBoolean(PrefKey.TITLE_FULLPATH));
        chkDisplayCategories.setSelected(LocalPrefs.getBoolean(PrefKey.DISPLAY_CATEGORIES));
        sldButtonSize.setValue(LocalPrefs.getDouble(PrefKey.SIZE_FACTOR));
        
	}

	/**
	 * Returns if user clicked ok.
	 *
	 * @return did user click ok?
	 */
	public boolean isOkClicked() {
        return okClicked;
    }

	/**
	 * Validates input, stores ok click, and closes dialog; does nothing for invalid input.
	 */
	@FXML
    private void handleOk() {
        okClicked = false;
        if (isInputValid()) {

    		// tab interval/color
        	LocalPrefs.putInt(PrefKey.INTERVAL, spnInterval.getValue());

        	LocalPrefs.putInt(PrefKey.PAST_FONTSIZE, spnPastFontsize.getValue());
        	LocalPrefs.put(PrefKey.PAST_FOREGROUND, ColorUtils.formatWebHex(pckPastForeground.getValue()));
        	LocalPrefs.put(PrefKey.PAST_BACKGROUND, ColorUtils.formatWebHex(pckPastBackground.getValue()));

        	LocalPrefs.putInt(PrefKey.PRESENT_FONTSIZE, spnPresentFontsize.getValue());
        	LocalPrefs.put(PrefKey.PRESENT_FOREGROUND, ColorUtils.formatWebHex(pckPresentForeground.getValue()));
        	LocalPrefs.put(PrefKey.PRESENT_BACKGROUND, ColorUtils.formatWebHex(pckPresentBackground.getValue()));

        	LocalPrefs.putInt(PrefKey.FUTURE_FONTSIZE, spnFutureFontsize.getValue());
        	LocalPrefs.put(PrefKey.FUTURE_FOREGROUND, ColorUtils.formatWebHex(pckFutureForeground.getValue()));
        	LocalPrefs.put(PrefKey.FUTURE_BACKGROUND, ColorUtils.formatWebHex(pckFutureBackground.getValue()));

    		// tab disable
        	boxCategories.getChildren()
					.forEach(checkbox -> {
						LocalPrefs.put(String.format(PREF_CATEGORY, ((CheckBox) checkbox).getText()), Boolean.toString(((CheckBox) checkbox).isSelected()));
					});
	
        	boxEventtypes.getChildren()
					.forEach(checkbox -> {
						LocalPrefs.put(String.format(PREF_EVENTTYPE, ((CheckBox) checkbox).getText()), Boolean.toString(((CheckBox) checkbox).isSelected()));
					});
        	
            // tab display
        	LocalPrefs.putBoolean(PrefKey.TITLE_FULLPATH, chkTitleFullpath.isSelected());
        	LocalPrefs.putBoolean(PrefKey.DISPLAY_CATEGORIES, chkDisplayCategories.isSelected());
        	LocalPrefs.putDouble(PrefKey.SIZE_FACTOR, sldButtonSize.getValue());
	
            okClicked = true;
            dialogStage.close();
        }
    }

	/**
	 * Stores non-ok click and closes dialog.
	 */
	@FXML
    private void handleCancel() {
        okClicked = false;
        dialogStage.close();
    }

	/**
	 * Validates input, shows error message for invalid input.
	 *
	 * @return is input valid?
	 */
	private boolean isInputValid() {

        StringBuilder sbErrorMessage = new StringBuilder();

        if (spnInterval.getValue() == null) {
            sbErrorMessage.append("Kein gültiges Intervall.\n");
        }

        if (spnPastFontsize.getValue() == null) {
            sbErrorMessage.append("Keine gültige Schriftgröße für vergangene Ereignisse.\n");
        }

        if (spnPresentFontsize.getValue() == null) {
            sbErrorMessage.append("Keine gültige Schriftgröße für heutige Ereignisse.\n");
        }

        if (spnFutureFontsize.getValue() == null) {
            sbErrorMessage.append("Keine gültige Schriftgröße für zukünftige Ereignisse.\n");
        }

        if (sbErrorMessage.length() == 0) {
            return true;
        }

        // Show the error message.
        AlertUtils.createAlert(AlertType.ERROR, dialogStage,
        		"Ungültige Eingaben",
        		"Bitte korrigieren Sie die fehlerhaften Eingaben.",
        		sbErrorMessage.toString())
        .showAndWait();

        return false;

    }

	/**
	 * Imports preferences.
	 */
	@FXML
	private void handleImport() {

		Alert alert = AlertUtils.createAlert(AlertType.CONFIRMATION, dialogStage,
				"Einstellungsimport",
				"Überschreiben von Einstellungen.",
				"Wenn Sie Einstellungen importieren, werden die bisherigen Einstellungen überschrieben, wenn Einstellungen dafür vorhanden sind.\nWollen Sie fortfahren?");

		alert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent()) {
			if (result.get() == ButtonType.YES) {
				FileChooser fileChooser = new FileChooser();

				fileChooser.setTitle("Einstellungen importieren");
				fileChooser.getExtensionFilters().addAll(
						new FileChooser.ExtensionFilter("Referee-Manager-Einstellungen (*.prefs)", "*.prefs"),
						new FileChooser.ExtensionFilter("Alle Dateien (*.*)", "*.*")
						);
				if (!LocalPrefs.get(PrefKey.PREFERENCES_FILE).isEmpty()) {
					Path pathPrefs = Paths.get(LocalPrefs.get(PrefKey.PREFERENCES_FILE));
					fileChooser.setInitialDirectory(pathPrefs.getParent().toFile());
					fileChooser.setInitialFileName(pathPrefs.getFileName().toString());
				}

				File flePrefs = fileChooser.showOpenDialog(dialogStage);

				if (flePrefs != null) {
					LocalPrefs.put(PrefKey.PREFERENCES_FILE, flePrefs.getAbsolutePath());

					try (InputStream stmIn = new FileInputStream(flePrefs)) {
						Prefs.importPrefs(stmIn);
						fillValues();
					} catch (IOException | InvalidPreferencesFormatException | BackingStoreException e) {
						AlertUtils.createAlert(AlertType.ERROR, dialogStage,
								"Importfehler",
								"Beim Import der Einstellungen ist ein Fehler aufgetreten.",
								MessageFormat.format("{0}\nDie Daten wurden nicht importiert.", e.getMessage()))
						.showAndWait();
					}
				}

			}
		}

	}

	/**
	 * Exports preferences.
	 */
	@FXML
	private void handleExport() {

		FileChooser fileChooser = new FileChooser();

		fileChooser.setTitle("Einstellungen exportieren");
		fileChooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("Referee-Manager-Einstellungen (*.prefs)", "*.prefs"),
				new FileChooser.ExtensionFilter("Alle Dateien (*.*)", "*.*")
				);
		if (!LocalPrefs.get(PrefKey.PREFERENCES_FILE).isEmpty()) {
			Path pathPrefs = Paths.get(LocalPrefs.get(PrefKey.PREFERENCES_FILE));
			if (pathPrefs.getParent().toFile().exists()) {
				fileChooser.setInitialDirectory(pathPrefs.getParent().toFile());
			}
			fileChooser.setInitialFileName(pathPrefs.getFileName().toString());
		}

		File flePrefs = fileChooser.showSaveDialog(dialogStage);

		if (flePrefs != null) {
			if (!flePrefs.getName().contains(".")) {
				flePrefs = new File(String.format("%s.prefs", flePrefs.getPath()));
			}
			LocalPrefs.put(PrefKey.PREFERENCES_FILE, flePrefs.getAbsolutePath());

			try (OutputStream stmOut = new FileOutputStream(flePrefs)) {
				Prefs.exportPrefs(stmOut);
			} catch (IOException | BackingStoreException e) {
				AlertUtils.createAlert(AlertType.ERROR, dialogStage,
						"Exportfehler",
						"Beim Export der Einstellungen ist ein Fehler aufgetreten.",
						MessageFormat.format("{0}\nDie Daten wurden nicht exportiert.", e.getMessage()))
				.showAndWait();
			}
		}

	}

}

/* EOF */
