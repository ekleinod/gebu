package de.edgesoft.gebu.controller;

import java.time.LocalDate;
import java.util.Map;

import de.edgesoft.edgeutils.datetime.DateTimeUtils;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.javafx.SceneUtils;
import de.edgesoft.gebu.Gebu;
import de.edgesoft.gebu.jaxb.Event;
import de.edgesoft.gebu.model.AppModel;
import de.edgesoft.gebu.model.ContentModel;
import de.edgesoft.gebu.model.EventModel;
import de.edgesoft.gebu.utils.AlertUtils;
import de.edgesoft.gebu.utils.LocalPrefs;
import de.edgesoft.gebu.utils.PrefKey;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Controller for event overview scene.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
public class EventEditorController {

	/**
	 * Table view.
	 */
	@FXML
	private TableView<Event> tblEvents;

	/**
	 * Title column.
	 */
	@FXML
	private TableColumn<EventModel, String> colTitle;

	/**
	 * Date column.
	 */
	@FXML
	private TableColumn<EventModel, LocalDate> colDate;

	/**
	 * Eventtype column.
	 */
	@FXML
	private TableColumn<EventModel, String> colEventtype;

	/**
	 * Category column.
	 */
	@FXML
	private TableColumn<EventModel, String> colCategory;

	/**
	 * Title label.
	 */
	@FXML
	private Label lblTitle;

	/**
	 * Date label.
	 */
	@FXML
	private Label lblDate;

	/**
	 * Eventtype label.
	 */
	@FXML
	private Label lblEventtype;

	/**
	 * Category label.
	 */
	@FXML
	private Label lblCategory;

	/**
	 * New button.
	 */
	@FXML
	private Button btnNew;

	/**
	 * Edit button.
	 */
	@FXML
	private Button btnEdit;

	/**
	 * Delete button.
	 */
	@FXML
	private Button btnDelete;

	/**
	 * Split pane.
	 */
	@FXML
	private SplitPane pneSplit;


	/**
	 * Main app controller.
	 */
	private AppLayoutController appController = null;
	

	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

		// set "empty data" text
		Label lblPlaceholder = new Label("Es wurden noch keine Ereignisse eingegeben. Geben Sie Ereignisse ein oder öffnen Sie eine vorhandene Datei über \"Datei->öffnen\"");
		lblPlaceholder.setWrapText(true);
		tblEvents.setPlaceholder(lblPlaceholder);

		// hook data to columns
		colTitle.setCellValueFactory(cellData -> cellData.getValue().getTitle());
		colDate.setCellValueFactory(cellData -> cellData.getValue().getDate());
		colEventtype.setCellValueFactory(cellData -> cellData.getValue().getEventtype());
		colCategory.setCellValueFactory(cellData -> cellData.getValue().getCategory());

		// format date column
		colDate.setCellFactory(column -> {
		    return new TableCell<>() {
		        @Override
		        protected void updateItem(LocalDate item, boolean empty) {
		            super.updateItem(item, empty);

		            if (item == null || empty) {
		                setText(null);
		            } else {
		                setText(DateTimeUtils.formatDate(item));
		            }
		        }
		    };
		});

		// set sorting of date column
		colDate.setComparator(EventModel.LOCALDATE);

		// clear event details
		showEventDetails(null);

		// listen to selection changes, show event
		tblEvents.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showEventDetails(newValue));
		tblEvents.setOnMousePressed(new EventHandler<MouseEvent>() {
		    @Override
		    public void handle(MouseEvent event) {
		        if (event.isPrimaryButtonDown() && (event.getClickCount() == 2)) {
		            handleEditEvent();
		        }
		    }
		});
		tblEvents.setOnKeyReleased(new EventHandler<KeyEvent>() {
		    @Override
		    public void handle(KeyEvent event) {
		        if (event.getCode() == KeyCode.ENTER) {
		            handleEditEvent();
		        }
		    }
		});

		// enabling edit/delete buttons only with selection
		btnEdit.disableProperty().bind(tblEvents.getSelectionModel().selectedItemProperty().isNull());
		btnDelete.disableProperty().bind(tblEvents.getSelectionModel().selectedItemProperty().isNull());

		// set divider position
		pneSplit.setDividerPositions(Double.parseDouble(LocalPrefs.get(PrefKey.STAGE_SPLIT)));

		// if changed, save divider position to preferences
		pneSplit.getDividers().get(0).positionProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			LocalPrefs.put(PrefKey.STAGE_SPLIT, Double.toString(newValue.doubleValue()));
		});

		// icons
		btnNew.setGraphic(new ImageView(Resources.loadImage("icons/actions/list-add-16.svg")));
		btnEdit.setGraphic(new ImageView(Resources.loadImage("icons/actions/edit-16.svg")));
		btnDelete.setGraphic(new ImageView(Resources.loadImage("icons/actions/list-remove-16.svg")));
		
	}

	/**
	 * Initializes the controller with things, that cannot be done during {@link #initialize()}.
	 * 
	 * @param theAppController app controller
	 */
	public void initController(final AppLayoutController theAppController) {

		appController = theAppController;
		
	}
		
	/**
	 * Sets events as table items.
	 */
	public void setTableItems() {
		if (AppModel.getData() != null) {
			tblEvents.setItems(((ContentModel) AppModel.getData().getContent()).getObservableEvents());
			tblEvents.refresh();
		} else {
			tblEvents.setItems(null);
			tblEvents.refresh();
		}
    }

	/**
	 * Shows selected event data in detail window.
	 *
	 * @param theEvent event (null if none is selected)
	 */
	private void showEventDetails(final Event theEvent) {

	    if (theEvent == null) {

	        lblTitle.setText("");
	        lblDate.setText("");
	        lblEventtype.setText("");
	        lblCategory.setText("");

	    } else {

	        lblTitle.setText(
	        		(theEvent.getTitle() == null) ?
	        				null :
	        				theEvent.getTitle().getValue());
	        lblDate.setText(
	        		(theEvent.getDate() == null) ?
	        				null :
	        				DateTimeUtils.formatDate((LocalDate) theEvent.getDate().getValue()));
	        lblEventtype.setText(
	        		(theEvent.getEventtype() == null) ?
	        				null :
	        				theEvent.getEventtype().getValue());
	        lblCategory.setText(
	        		(theEvent.getCategory() == null) ?
	        				null :
	        				theEvent.getCategory().getValue());

	    }

	}

	/**
	 * Opens edit dialog for new event.
	 */
	@FXML
	private void handleNewEvent() {

		EventModel newEvent = new EventModel();
		if (showEventEditDialog(newEvent)) {
			((ContentModel) AppModel.getData().getContent()).getObservableEvents().add(newEvent);
			tblEvents.getSelectionModel().select(newEvent);
			AppModel.setModified(true);
			appController.setAppTitle();
		}

	}

	/**
	 * Opens edit dialog for editing selected event.
	 */
	@FXML
	private void handleEditEvent() {

		EventModel editEvent = (EventModel) tblEvents.getSelectionModel().getSelectedItem();

	    if (editEvent != null) {

			if (showEventEditDialog(editEvent)) {
				showEventDetails(editEvent);
				AppModel.setModified(true);
				appController.setAppTitle();
			}

	    }

	}

	/**
	 * Deletes selected event from list.
	 */
	@FXML
	private void handleDeleteEvent() {

	    int selectedIndex = tblEvents.getSelectionModel().getSelectedIndex();

	    if (selectedIndex >= 0) {

	    	Alert alert = AlertUtils.createAlert(AlertType.CONFIRMATION, appController.getPrimaryStage(),
	    			"Bestätigung Ereignis löschen",
	    			"Soll das ausgewählte Ereignis gelöscht werden?",
	    			null);

	        alert.showAndWait()
	        		.filter(response -> response == ButtonType.OK)
	        		.ifPresent(response -> {
	        			tblEvents.getItems().remove(selectedIndex);
	        			AppModel.setModified(true);
	        			appController.setAppTitle();
	        			});

	    }

	}

	/**
	 * Opens the event edit dialog.
	 *
	 * If the user clicks OK, the changes are saved into the provided event object and true is returned.
	 *
	 * @param theEvent the event to be edited
	 * @return true if the user clicked OK, false otherwise.
	 */
	private boolean showEventEditDialog(Event theEvent) {

    	Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("EventEditDialog");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(appController.getPrimaryStage());
        dialogStage.setTitle("Ereignis editieren");

        dialogStage.setScene(SceneUtils.createScene(pneLoad.getKey(), Gebu.CSS));

        // Set the event into the controller.
        EventEditDialogController editController = pneLoad.getValue().getController();
        editController.setDialogStage(dialogStage);
        editController.setEvent(theEvent);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        return editController.isOkClicked();

	}

}

/* EOF */
