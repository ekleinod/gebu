package de.edgesoft.gebu.controller;

import java.io.File;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;

import de.edgesoft.edgeutils.EdgeUtilsException;
import de.edgesoft.edgeutils.commons.Info;
import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.javafx.SceneUtils;
import de.edgesoft.edgeutils.jaxb.JAXBFiles;
import de.edgesoft.gebu.Gebu;
import de.edgesoft.gebu.jaxb.Content;
import de.edgesoft.gebu.jaxb.Event;
import de.edgesoft.gebu.jaxb.ObjectFactory;
import de.edgesoft.gebu.model.AppModel;
import de.edgesoft.gebu.model.ContentModel;
import de.edgesoft.gebu.utils.AlertUtils;
import de.edgesoft.gebu.utils.LocalPrefs;
import de.edgesoft.gebu.utils.PrefKey;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Controller for application layout.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
public class AppLayoutController {

	/**
	 * Application icon.
	 */
	public static final Image ICON = Resources.loadImage("images/icon-32.svg");

	/**
	 * App border pane.
	 */
	@FXML
	private BorderPane appPane;

	/**
	 * Menu item program -> display.
	 */
	@FXML
	private MenuItem mnuProgramDisplay;

	/**
	 * Menu item program -> editor.
	 */
	@FXML
	private MenuItem mnuProgramEditor;

	/**
	 * Menu item program -> preferences.
	 */
	@FXML
	private MenuItem mnuProgramPreferences;

	/**
	 * Menu item program -> quit.
	 */
	@FXML
	private MenuItem mnuProgramQuit;

	/**
	 * Menu file.
	 */
	@FXML
	private Menu mnuFile;

	/**
	 * Menu item file -> new.
	 */
	@FXML
	private MenuItem mnuFileNew;

	/**
	 * Menu item file -> open.
	 */
	@FXML
	private MenuItem mnuFileOpen;

	/**
	 * Menu item file -> save.
	 */
	@FXML
	private MenuItem mnuFileSave;

	/**
	 * Menu item file -> save as.
	 */
	@FXML
	private MenuItem mnuFileSaveAs;

	/**
	 * Menu statistics.
	 */
	@FXML
	private Menu mnuStatistics;

	/**
	 * Menu item statistics -> data.
	 */
	@FXML
	private MenuItem mnuStatisticsData;

	/**
	 * Menu item help -> manual.
	 */
	@FXML
	private MenuItem mnuHelpManual;

	/**
	 * Menu item help -> about.
	 */
	@FXML
	private MenuItem mnuHelpAbout;

	/**
	 * Main toolbar.
	 */
	@FXML
	private ToolBar barMain;

	/**
	 * Button program -> quit.
	 */
	@FXML
	private Button btnProgramQuit;

	/**
	 * Button file -> new.
	 */
	@FXML
	private Button btnFileNew;

	/**
	 * Button file -> open.
	 */
	@FXML
	private Button btnFileOpen;

	/**
	 * Button file -> save.
	 */
	@FXML
	private Button btnFileSave;

	/**
	 * Button file -> save as.
	 */
	@FXML
	private Button btnFileSaveAs;

	/**
	 * Button statistics -> data.
	 */
	@FXML
	private Button btnStatisticsData;


	/**
	 * Primary stage.
	 */
	private Stage primaryStage = null;

	/**
	 * Event overview controller.
	 */
	private EventEditorController ctlEventOverview = null;

	/**
	 * Flag, if display (true) or editor (false) is shown.
	 */
	private BooleanProperty isDisplay = null;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

		isDisplay = new SimpleBooleanProperty();

		// icons
		mnuProgramDisplay.setGraphic(new ImageView(Resources.loadImage("icons/actions/view-calendar-birthday.svg")));
		mnuProgramEditor.setGraphic(new ImageView(Resources.loadImage("icons/actions/document-edit.svg")));
		mnuProgramPreferences.setGraphic(new ImageView(Resources.loadImage("icons/actions/configure.svg")));
		mnuProgramQuit.setGraphic(new ImageView(Resources.loadImage("icons/actions/application-exit.svg")));

		mnuFileNew.setGraphic(new ImageView(Resources.loadImage("icons/actions/document-new.svg")));
		mnuFileOpen.setGraphic(new ImageView(Resources.loadImage("icons/actions/document-open.svg")));
		mnuFileSave.setGraphic(new ImageView(Resources.loadImage("icons/actions/document-save.svg")));
		mnuFileSaveAs.setGraphic(new ImageView(Resources.loadImage("icons/actions/document-save-as.svg")));

		mnuStatisticsData.setGraphic(new ImageView(Resources.loadImage("icons/actions/office-chart-bar.svg")));

		mnuHelpManual.setGraphic(new ImageView(Resources.loadImage("icons/actions/help-about.svg")));
		mnuHelpAbout.setGraphic(new ImageView(Resources.loadImage("icons/actions/help-about.svg")));

		// toolbar
		btnProgramQuit.setGraphic(new ImageView(Resources.loadImage("icons/actions/application-exit.svg")));

		btnFileNew.setGraphic(new ImageView(Resources.loadImage("icons/actions/document-new.svg")));
		btnFileOpen.setGraphic(new ImageView(Resources.loadImage("icons/actions/document-open.svg")));
		btnFileSave.setGraphic(new ImageView(Resources.loadImage("icons/actions/document-save.svg")));
		btnFileSaveAs.setGraphic(new ImageView(Resources.loadImage("icons/actions/document-save-as.svg")));

		btnStatisticsData.setGraphic(new ImageView(Resources.loadImage("icons/actions/office-chart-bar.svg")));

        // bind menu/toolbar enabling to display kind
		mnuProgramDisplay.disableProperty().bind(isDisplay);
		mnuProgramEditor.disableProperty().bind(isDisplay.not());

		// hide unneeded menus/toolbars, disable menu items, so they cannot be used in display mode
		mnuFile.visibleProperty().bind(isDisplay.not());
		mnuFile.getItems().stream()
				.forEach(menuitem -> menuitem.disableProperty().bind(isDisplay));

		mnuStatistics.visibleProperty().bind(isDisplay.not());
		mnuStatistics.getItems().stream()
				.forEach(menuitem -> menuitem.disableProperty().bind(isDisplay));

		barMain.visibleProperty().bind(isDisplay.not());
		barMain.managedProperty().bind(barMain.visibleProperty());
		barMain.getItems().stream()
				.forEach(button -> button.disableProperty().bind(isDisplay));

	}

	/**
	 * Initializes the controller with things, that cannot be done during {@link #initialize()}.
	 *
	 * @param thePrimaryStage primary stage
	 */
	public void initController(final Stage thePrimaryStage) {

		primaryStage = thePrimaryStage;

        // set icon
		primaryStage.getIcons().add(ICON);

        // Show the scene containing the root layout.
        primaryStage.setScene(SceneUtils.createScene(appPane, Gebu.CSS));
        primaryStage.show();

        // resize to last dimensions
    	primaryStage.setX(LocalPrefs.getDouble(PrefKey.STAGE_X));
    	primaryStage.setY(LocalPrefs.getDouble(PrefKey.STAGE_Y));

    	primaryStage.setWidth(LocalPrefs.getDouble(PrefKey.STAGE_WIDTH));
    	primaryStage.setHeight(LocalPrefs.getDouble(PrefKey.STAGE_HEIGHT));

    	primaryStage.setMaximized(LocalPrefs.getBoolean(PrefKey.MAXIMIZED));

		// if changed, save bounds to preferences
		primaryStage.xProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if (!primaryStage.isMaximized()) {
				LocalPrefs.putDouble(PrefKey.STAGE_X, newValue.doubleValue());
			}
		});
		primaryStage.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if (!primaryStage.isMaximized()) {
				LocalPrefs.putDouble(PrefKey.STAGE_WIDTH, newValue.doubleValue());
			}
		});
		primaryStage.yProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if (!primaryStage.isMaximized()) {
				LocalPrefs.putDouble(PrefKey.STAGE_Y, newValue.doubleValue());
			}
		});
		primaryStage.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			if (!primaryStage.isMaximized()) {
				LocalPrefs.putDouble(PrefKey.STAGE_HEIGHT, newValue.doubleValue());
			}
		});

		primaryStage.maximizedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			LocalPrefs.putBoolean(PrefKey.MAXIMIZED, newValue.booleanValue());
		});

        // set handler for close requests (x-button of window)
		primaryStage.setOnCloseRequest(event -> {
        	event.consume();
        	handleProgramExit();
        });

		// finally, we can initialize the data
		initData();

		// show correct pane
        if (AppModel.getData().getContent().getEvent().isEmpty()) {
        	handleProgramEditor();
        } else {
        	handleProgramDisplay();
        }

    }

	/**
	 * Initializes the data.
	 */
	private void initData() {

		if (AppModel.getFilename().isEmpty()) {
			newData();
		} else {
			openData(LocalPrefs.get(PrefKey.FILE));
		}

    }

	/**
	 * Creates new data.
	 */
	private void newData() {

		de.edgesoft.gebu.jaxb.Gebu dtaGebu = new ObjectFactory().createGebu();

		Info info = new de.edgesoft.edgeutils.commons.ObjectFactory().createInfo();

		info.setCreated(LocalDateTime.now());
		info.setModified(LocalDateTime.now());
		info.setAppversion(Gebu.getVersion());
		info.setDocversion(Gebu.getVersion());
		info.setCreator(Gebu.class.getCanonicalName());

		dtaGebu.setInfo(info);

		Content content = new ObjectFactory().createContent();
		dtaGebu.setContent(content);

		AppModel.setData(dtaGebu);

		AppModel.setFilename(null);
		AppModel.setModified(false);
		AppModel.setLegacy(false);
		setAppTitle();

		if (ctlEventOverview != null) {
			ctlEventOverview.setTableItems();
		}

    }

	/**
	 * Sets the app title.
	 */
	public void setAppTitle() {

		String sFile = LocalPrefs.get(PrefKey.FILE).isEmpty() ?
				"" :
				String.format(" - %s", Boolean.parseBoolean(LocalPrefs.get(PrefKey.TITLE_FULLPATH)) ?
						Paths.get(LocalPrefs.get(PrefKey.FILE)).toAbsolutePath().toString() :
						Paths.get(LocalPrefs.get(PrefKey.FILE)).getFileName().toString());

		primaryStage.setTitle(String.format("Das Gebu-Programm%s%s",
				sFile,
				AppModel.isModified() ? " *" : ""
				));

    }

	/**
	 * Loads data from the given file.
	 *
	 * @param theFilename filename
	 */
	private void openData(final String theFilename) {

		try {

			de.edgesoft.gebu.jaxb.Gebu dtaGebu = JAXBFiles.unmarshal(theFilename, de.edgesoft.gebu.jaxb.Gebu.class);

			AppModel.setData(dtaGebu);
			AppModel.setLegacy(false);

			// legacy files?
			if (dtaGebu.getInfo() == null) {
				openLegacyData(theFilename);
			}

			if (ctlEventOverview != null) {
				ctlEventOverview.setTableItems();
			}

			AppModel.setFilename(theFilename);
			AppModel.setModified(false);
			setAppTitle();

		} catch (EdgeUtilsException e) {

	        AlertUtils.createAlert(AlertType.ERROR, primaryStage,
	        		"Datenfehler",
	        		"Ein Fehler ist beim Laden der Gebu-Daten aufgetreten.",
	        		MessageFormat.format("{0}\nDas Programm wird ohne Daten fortgeführt.", e.getMessage()))
	        .showAndWait();

	        newData();

		}

    }

	/**
	 * Loads and converts legacy data.
	 *
	 * @param theFilename filename
	 *
	 * @throws EdgeUtilsException if loading or converting went wrong
	 */
	private void openLegacyData(final String theFilename) throws EdgeUtilsException {

		newData();

		de.edgesoft.gebu.jaxb_legacy_5_2.Gebu dtaLegacy = JAXBFiles.unmarshal(theFilename, de.edgesoft.gebu.jaxb_legacy_5_2.Gebu.class);

		dtaLegacy.getData().getEvent().stream().forEach(
				event -> {

					Event newEvent = new ObjectFactory().createEvent();

					newEvent.setTitle(new SimpleStringProperty(event.getDescription()));
					newEvent.setDate(new SimpleObjectProperty<>(event.getDate()));
					newEvent.setEventtype(new SimpleStringProperty(event.getEventname()));
					newEvent.setCategory(new SimpleStringProperty((event.getCategory().equals("Keine") ? null : event.getCategory())));

					AppModel.getData().getContent().getEvent().add(newEvent);
					AppModel.setLegacy(true);

				}
				);

        AlertUtils.createAlert(AlertType.INFORMATION, primaryStage,
        		"Datenkonvertierung",
        		null,
        		"Die eingelesenen Daten stammen von einer alten Programmversion. Die Daten wurden eingelesen und konvertiert.\n\nFalls Sie die Daten speichern, werden diese im neuen Format gespeichert. Wenn Sie die Originaldatei behalten wollen, speichern Sie die Datei nicht oder unter einem anderen Namen ab.")
        .showAndWait();

    }

	/**
	 * Program menu display.
	 *
	 * Shows the event display.
	 */
	@FXML
	private void handleProgramDisplay() {

    	Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("EventDisplay");
    	AnchorPane eventDisplay = (AnchorPane) pneLoad.getKey();

        // Set event overview into the center of root layout.
        appPane.setCenter(eventDisplay);
        eventDisplay.requestFocus();
        
        // Give the controller access to the app.
        EventDisplayController ctlEventDisplay = pneLoad.getValue().getController();
        ctlEventDisplay.initController(this);
        ctlEventDisplay.displayEvents(LocalDate.now());

        isDisplay.setValue(true);

	}

	/**
	 * Program menu editor.
	 *
	 * Shows the event overview.
	 */
	@FXML
	private void handleProgramEditor() {

    	Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("EventEditor");
    	AnchorPane eventOverview = (AnchorPane) pneLoad.getKey();

        // Set event overview into the center of root layout.
        appPane.setCenter(eventOverview);
        
        // Give the controller access to the app.
        ctlEventOverview = pneLoad.getValue().getController();
        ctlEventOverview.initController(this);
        ctlEventOverview.setTableItems();

        isDisplay.setValue(false);

	}

	/**
	 * Program menu preferences.
	 *
	 * Opens the preferences edit dialog.
	 */
	@FXML
	private void handleProgramPreferences() {

    	Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("PreferencesEditDialog");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Einstellungen");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);

        dialogStage.setScene(SceneUtils.createScene(pneLoad.getKey(), Gebu.CSS));

        // initialize controller
        PreferencesEditDialogController controller = pneLoad.getValue().getController();
        controller.setDialogStage(dialogStage);

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

        // reload display after changing preferences
        if (isDisplay.getValue() && controller.isOkClicked()) {
        	handleProgramDisplay();
        }
        setAppTitle();

	}

	/**
	 * Program menu exit.
	 */
	@FXML
	public void handleProgramExit() {
		if (checkModified()) {
			Platform.exit();
		}
	}

	/**
	 * File menu new.
	 */
	@FXML
	private void handleFileNew() {
		if (checkModified()) {
			newData();
		}
	}

	/**
	 * File menu open.
	 */
	@FXML
	private void handleFileOpen() {

		if (checkModified()) {

			FileChooser fileChooser = new FileChooser();

			fileChooser.setTitle("Gebu-Datei öffnen");
	        fileChooser.getExtensionFilters().addAll(
	        		new FileChooser.ExtensionFilter("Gebu-Dateien (*.gebu)", "*.gebu"),
	        		new FileChooser.ExtensionFilter("Gebu-5-Dateien (*.esx)", "*.esx"),
	        		new FileChooser.ExtensionFilter("Alle Dateien (*.*)", "*.*")
	        		);
	        if (!LocalPrefs.get(PrefKey.PATH).isEmpty()) {
	        	fileChooser.setInitialDirectory(new File(LocalPrefs.get(PrefKey.PATH)));
	        }

	        File file = fileChooser.showOpenDialog(primaryStage);

	        if (file != null) {
	            openData(file.getPath());
	        }

		}

	}

	/**
	 * File menu save.
	 */
	@FXML
    public void handleFileSave() {
        if (LocalPrefs.get(PrefKey.FILE).isEmpty() || AppModel.isLegacy()) {
        	handleFileSaveAs();
        } else {
        	saveData(LocalPrefs.get(PrefKey.FILE));
        }
    }

	/**
	 * File menu save as.
	 */
	@FXML
    private void handleFileSaveAs() {

		FileChooser fileChooser = new FileChooser();

		fileChooser.setTitle("Gebu-Datei speichern");
        fileChooser.getExtensionFilters().addAll(
        		new FileChooser.ExtensionFilter("Gebu-Dateien (*.gebu)", "*.gebu"),
        		new FileChooser.ExtensionFilter("Alle Dateien (*)", "*")
        		);
        if (!LocalPrefs.get(PrefKey.PATH).isEmpty()) {
        	fileChooser.setInitialDirectory(new File(LocalPrefs.get(PrefKey.PATH)));
        }

        File file = fileChooser.showSaveDialog(primaryStage);

        if (file != null) {
            saveData(file.getPath());
        }

    }

	/**
	 * Help menu manual.
	 */
	@FXML
    private void handleHelpManual() {

        Alert alert = AlertUtils.createAlert(AlertType.INFORMATION, primaryStage,
        		"Das Gebu-Programm - das Handbuch",
        		MessageFormat.format("Das Gebu-Programm Version {0}", Gebu.getVersion()),
        		null
        		);

    	Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("ManualText");
    	alert.getDialogPane().contentProperty().set(pneLoad.getKey());

        alert.setGraphic(new ImageView(Resources.loadImage("images/logo.svg", 64)));
        alert.showAndWait();

    }

	/**
	 * Help menu about.
	 */
	@FXML
    private void handleHelpAbout() {

        Alert alert = AlertUtils.createAlert(AlertType.INFORMATION, primaryStage,
        		"Über \"Das Gebu-Programm\"",
        		MessageFormat.format("Das Gebu-Programm Version {0}", Gebu.getVersion()),
        		null
        		);

    	Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("AboutText");
    	alert.getDialogPane().contentProperty().set(pneLoad.getKey());

        alert.setGraphic(new ImageView(Resources.loadImage("images/logo.svg", 64)));
        alert.showAndWait();

    }

	/**
	 * Event menu statistics.
	 *
	 * Shows the event statistics.
	 */
	@FXML
    private void handleEventStatistics() {

    	Map.Entry<Parent, FXMLLoader> pneLoad = Resources.loadNode("EventStatistics");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Ereignisstatistik");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);

        dialogStage.setScene(SceneUtils.createScene(pneLoad.getKey(), Gebu.CSS));

        // Set the events into the controller.
        EventStatisticsController controller = pneLoad.getValue().getController();
        controller.fillStatistics(AppModel.getData().getContent().getEvent());
        controller.setDialogStage(dialogStage);

        // Show the dialog and wait until the user closes it
        dialogStage.show();

    }

	/**
	 * Check if data is modified, show corresponding dialog, save data if needed.
	 *
	 * @return did user select continue (true) or cancel (false)?
	 */
	private boolean checkModified() {

		boolean doContinue = true;

		if (AppModel.isModified() || AppModel.isLegacy()) {

	    	Alert alert = AlertUtils.createAlert(AlertType.CONFIRMATION, primaryStage,
	    			"Nicht gespeicherte Änderungen",
	    			"Sie haben Änderungen durchgeführt, die noch nicht gespeichert wurden.",
	    			"Wollen Sie die geänderten Daten speichern?");

	        alert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);

	        Optional<ButtonType> result = alert.showAndWait();
	        if (result.isPresent()) {
    			if (result.get() == ButtonType.YES) {
    				handleFileSave();
    				doContinue = true;
    			}
    			if (result.get() == ButtonType.NO) {
    				doContinue = true;
    			}
    			if (result.get() == ButtonType.CANCEL) {
    				doContinue = false;
    			}
	        }

		}

		return doContinue;

	}

	/**
	 * Saves the data.
	 *
	 * @param theFilename filename
	 */
	private void saveData(final String theFilename) {

		try {

			AppModel.getData().getInfo().setModified(LocalDateTime.now());
			AppModel.getData().getInfo().setAppversion(Gebu.getVersion());
			AppModel.getData().getInfo().setDocversion(Gebu.getVersion());
			AppModel.getData().getInfo().setCreator(Gebu.class.getCanonicalName());

			((ContentModel) AppModel.getData().getContent()).sortEvents();

			if (ctlEventOverview != null) {
				ctlEventOverview.setTableItems();
			}

			JAXBFiles.marshal(new ObjectFactory().createGebu(AppModel.getData()), theFilename, null);

			AppModel.setFilename(theFilename);
			AppModel.setModified(false);
			AppModel.setLegacy(false);

		} catch (EdgeUtilsException e) {

	        AlertUtils.createAlert(AlertType.ERROR, primaryStage,
	        		"Datenfehler",
	        		"Ein Fehler ist beim Speichern der Gebu-Daten aufgetreten.",
	        		MessageFormat.format("{0}\nDie Daten wurden nicht gespeichert.", e.getMessage()))
	        .showAndWait();

		}

		setAppTitle();

    }

	/**
	 * Returns primary stage.
	 *
	 * @return primary stage
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}

}

/* EOF */
