package de.edgesoft.gebu.controller;

import java.time.LocalDate;
import java.util.Objects;

import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.javafx.EdgeDatePicker;
import de.edgesoft.gebu.jaxb.Event;
import de.edgesoft.gebu.model.AppModel;
import de.edgesoft.gebu.model.ContentModel;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * Controller for event edit dialog scene.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
public class EventEditDialogController {

	/**
	 * Title text field.
	 */
	@FXML
	private TextField txtTitle;

	/**
	 * Date picker.
	 */
	@FXML
	private EdgeDatePicker pckDate;

	/**
	 * Eventtype combobox.
	 */
	@FXML
	private ComboBox<String> cboEventtype;

	/**
	 * Category combobox.
	 */
	@FXML
	private ComboBox<String> cboCategory;

	/**
	 * OK button.
	 */
	@FXML
	private Button btnOK;

	/**
	 * Cancel button.
	 */
	@FXML
	private Button btnCancel;

	/**
	 * Reference to dialog stage.
	 */
	private Stage dialogStage;

	/**
	 * Current event.
	 */
	private Event currentEvent;

	/**
	 * OK clicked?.
	 */
	private boolean okClicked;


	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

		// enable ok button for valid entries only
		btnOK.disableProperty().bind(
				pckDate.valueProperty().isNull()
				.or(txtTitle.textProperty().isEmpty())
				.or(cboEventtype.valueProperty().isNull())
				.or(cboEventtype.valueProperty().asString().isEmpty())
				);

		// icons
		btnOK.setGraphic(new ImageView(Resources.loadImage("icons/actions/dialog-ok-16.svg")));
		btnCancel.setGraphic(new ImageView(Resources.loadImage("icons/actions/dialog-cancel-16.svg")));

	}

	/**
	 * Sets dialog stage.
	 *
	 * @param theStage dialog stage
	 */
	public void setDialogStage(final Stage theStage) {
        dialogStage = theStage;
    }

	/**
	 * Sets event to be edited.
	 *
	 * @param theEvent edit event
	 */
	public void setEvent(Event theEvent) {

		Objects.requireNonNull(theEvent);

        txtTitle.setText(
        		(theEvent.getTitle() == null) ?
        				null :
        				theEvent.getTitle().getValue());
        pckDate.setValue(
        		(theEvent.getDate() == null) ?
        				null :
        				(LocalDate) theEvent.getDate().getValue());
        cboEventtype.setValue(
        		(theEvent.getEventtype() == null) ?
        				null :
        				theEvent.getEventtype().getValue());
        cboCategory.setValue(
        		(theEvent.getCategory() == null) ?
        				null :
        				theEvent.getCategory().getValue());

        currentEvent = theEvent;

		// fill event type and category boxes
		cboEventtype.setItems(FXCollections.observableArrayList(((ContentModel) AppModel.getData().getContent()).getEventtypes()));
		cboCategory.setItems(FXCollections.observableArrayList(((ContentModel) AppModel.getData().getContent()).getCategories()));

    }

	/**
	 * Returns if user clicked ok.
	 *
	 * @return did user click ok?
	 */
	public boolean isOkClicked() {
        return okClicked;
    }

	/**
	 * Validates input, stores ok click, and closes dialog; does nothing for invalid input.
	 */
	@FXML
    private void handleOk() {
		
        okClicked = false;

    	if (currentEvent.getTitle() == null) {
    		currentEvent.setTitle(new SimpleStringProperty());
    	}
        currentEvent.getTitle().setValue(txtTitle.getText());

        if (currentEvent.getDate() == null) {
    		currentEvent.setDate(new SimpleObjectProperty<>());
    	}
        currentEvent.getDate().setValue(pckDate.getValue());

    	if (currentEvent.getEventtype() == null) {
    		currentEvent.setEventtype(new SimpleStringProperty());
    	}
        currentEvent.getEventtype().setValue(cboEventtype.getValue());

        if (currentEvent.getCategory() == null) {
    		currentEvent.setCategory(new SimpleStringProperty());
    	}
        currentEvent.getCategory().setValue(cboCategory.getValue());

        okClicked = true;
        dialogStage.close();

    }

	/**
	 * Stores non-ok click and closes dialog.
	 */
	@FXML
    private void handleCancel() {
		okClicked = false;
        dialogStage.close();
    }

}

/* EOF */
