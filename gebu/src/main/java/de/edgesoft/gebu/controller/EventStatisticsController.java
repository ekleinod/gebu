package de.edgesoft.gebu.controller;

import java.io.IOException;
import java.io.StringWriter;
import java.text.Collator;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import de.edgesoft.edgeutils.files.Resources;
import de.edgesoft.edgeutils.freemarker.TemplateHandler;
import de.edgesoft.gebu.Gebu;
import de.edgesoft.gebu.jaxb.Event;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.image.ImageView;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * Controller for event statistics scene.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
public class EventStatisticsController {

	/**
	 * Bar chart overview.
	 */
	@FXML
	private BarChart<String, Integer> chartOverview;

	/**
	 * Stacked bar chart overview.
	 */
	@FXML
	private StackedBarChart<String, Integer> chartStacked;

	/**
	 * Pie chart event types.
	 */
	@FXML
	private PieChart chartEventtypes;

	/**
	 * Details view.
	 */
	@FXML
	private WebView viewDetails;

	/**
	 * Tab overview.
	 */
	@FXML
	private Tab tabOverview;

	/**
	 * Tab stacked.
	 */
	@FXML
	private Tab tabStacked;

	/**
	 * Tab event types.
	 */
	@FXML
	private Tab tabEventtypes;

	/**
	 * Tab view.
	 */
	@FXML
	private Tab tabDetails;

	/**
	 * OK button.
	 */
	@FXML
	private Button btnOK;

	/**
	 * Reference to dialog stage.
	 */
	private Stage dialogStage;


	/**
	 * Sets dialog stage.
	 *
	 * @param theStage dialog stage
	 */
	public void setDialogStage(final Stage theStage) {
        dialogStage = theStage;
    }

	/**
	 * Initializes the controller class.
	 *
	 * This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

		// icons
		btnOK.setGraphic(new ImageView(Resources.loadImage("icons/actions/dialog-ok-16.svg")));

		tabOverview.setGraphic(new ImageView(Resources.loadImage("icons/actions/office-chart-bar.svg")));
		tabStacked.setGraphic(new ImageView(Resources.loadImage("icons/actions/office-chart-bar-stacked.svg")));
		tabEventtypes.setGraphic(new ImageView(Resources.loadImage("icons/actions/office-chart-pie.svg")));
		tabDetails.setGraphic(new ImageView(Resources.loadImage("icons/actions/view-list-details.svg")));

	}

	/**
	 * Fills statistics with event data.
	 *
	 * @param theEvents event data
	 */
	public void fillStatistics(final List<Event> theEvents) {

		// compute statistics data
		Map<String, Map<Month, AtomicInteger>> mapCounts = new HashMap<>();

		theEvents.forEach(event -> {
			mapCounts.computeIfAbsent(event.getEventtype().getValue(), eventtype -> {
				Map<Month, AtomicInteger> mapMonthCounts = new HashMap<>();
				for (Month month : Month.values()) {
					mapMonthCounts.put(month, new AtomicInteger());
				}
				return mapMonthCounts;
			});
			mapCounts.get(event.getEventtype().getValue()).get(((LocalDate) event.getDate().getValue()).getMonth()).incrementAndGet();
		});

		AtomicInteger iWholeCount = new AtomicInteger();
		Map<String, Object> mapContent = new HashMap<>();
		Map<String, Integer> mapStats = new HashMap<>();

		// output data
		mapCounts.entrySet().stream()
				.sorted(Comparator.comparing(typemap -> typemap.getKey(), Collator.getInstance()))
				.forEach(typemap -> {

					AtomicInteger iEventCount = new AtomicInteger();

					XYChart.Series<String, Integer> seriesOverview = new XYChart.Series<>();
					seriesOverview.setName(typemap.getKey());

					XYChart.Series<String, Integer> seriesStacked = new XYChart.Series<>();
					seriesStacked.setName(typemap.getKey());

					typemap.getValue().entrySet().stream()
							.sorted(Comparator.comparing(monthcount -> monthcount.getKey()))
									.forEach(monthcount -> {
										seriesOverview.getData().add(new XYChart.Data<>(monthcount.getKey().getDisplayName(TextStyle.FULL, Locale.getDefault()), monthcount.getValue().get()));
										seriesStacked.getData().add(new XYChart.Data<>(monthcount.getKey().getDisplayName(TextStyle.FULL, Locale.getDefault()), monthcount.getValue().get()));
										iEventCount.addAndGet(monthcount.getValue().get());
									});

					chartOverview.getData().add(seriesOverview);
					chartStacked.getData().add(seriesStacked);
					chartEventtypes.getData().add(new PieChart.Data(String.format("%s (%d)", typemap.getKey(), iEventCount.get()), iEventCount.get()));

					iWholeCount.addAndGet(iEventCount.get());

					mapStats.put(typemap.getKey(), iEventCount.get());

				});

		mapContent.put("stats", mapStats);
		mapContent.put("sum", iWholeCount);

		try {

			Template tplStatistics = TemplateHandler.loadTemplate(Gebu.class, "statisticsview.html");

			try (StringWriter wrtContent = new StringWriter()) {
				tplStatistics.process(mapContent, wrtContent);
				viewDetails.getEngine().loadContent(wrtContent.toString());
			}

		} catch (IOException | TemplateException e) {
            Gebu.logger.catching(e);
		}


    }

	/**
	 * Closes dialog.
	 */
	@FXML
    private void handleOk() {
        dialogStage.close();
    }

}

/* EOF */
