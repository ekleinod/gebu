package de.edgesoft.gebu.controller;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import de.edgesoft.edgeutils.datetime.DateTimeUtils;
import de.edgesoft.gebu.jaxb.Event;
import de.edgesoft.gebu.model.AppModel;
import de.edgesoft.gebu.model.ContentModel;
import de.edgesoft.gebu.model.EventModel;
import de.edgesoft.gebu.utils.LocalPrefs;
import de.edgesoft.gebu.utils.PrefKey;
import de.edgesoft.gebu.utils.TimeCategories;
import javafx.fxml.FXML;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;

/**
 * Controller for event display scene.
 * 
 * Getting the display right with means of JavaFX is quite difficult, thus the complicated constructs with panes, boxes etc. 
 * 
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
public class EventDisplayController {
	
	/**
	 * Grid pane for events.
	 */
	@FXML
	private GridPane grdEvents;

	/**
	 * Main app controller.
	 */
	private AppLayoutController appController = null;


	/**
	 * Initializes the controller with things, that cannot be done during {@link #initialize()}.
	 *
	 * @param theAppController app controller
	 */
	public void initController(final AppLayoutController theAppController) {

		appController = theAppController;
		
	}

	/**
	 * Displays events for given date.
	 * 
	 * @param theDate date to compare to
	 */
	public void displayEvents(
			final LocalDate theDate
			) {

		if (AppModel.getData().getContent().getEvent().isEmpty()) {
			grdEvents.addRow(grdEvents.getRowCount(), new Label("Es wurden noch keine Ereignisse eingegeben."));
			return;
		}
		
		boolean noEventsInInterval = true;
		int iInterval = LocalPrefs.getInt(PrefKey.INTERVAL);
		
		for (TimeCategories theTimeCategory : TimeCategories.values()) {
			
			List<Event> lstTemp = ((ContentModel) AppModel.getData().getContent()).getSortedFilterEvents(
					theDate, 
					theTimeCategory.getSign() * theTimeCategory.getIntervalStart().orElse(iInterval), 
					theTimeCategory.getSign() * theTimeCategory.getIntervalEnd().orElse(iInterval)
					);
			
			if (!lstTemp.isEmpty()) {
				lstTemp.stream().forEach(event -> addEventToGrid(event, theDate, theTimeCategory));
				noEventsInInterval = false;
			}
			
		}
		
		if (noEventsInInterval) {
			grdEvents.addRow(grdEvents.getRowCount(), new Label(MessageFormat.format(
					"Im Intervall von ± {0, choice, 0#0 Tagen|1#einem Tag|1<{0, number, integer} Tagen} liegen keine Ereignisse.", 
					iInterval)));
		}
		
	}

	/**
	 * Adding event label to grid pane.
	 * 
	 * @param theEvent event
	 * @param theDate comparison date
	 * @param theTimeCategory time category
	 */
	private void addEventToGrid(
			final Event theEvent,
			final LocalDate theDate,
			final TimeCategories theTimeCategory
			) {
		
		List<Label> lstLabels = new ArrayList<>();
		lstLabels.add(new Label(DateTimeUtils.formatDate((LocalDate) theEvent.getDate().getValue())));
		lstLabels.add(new Label(MessageFormat.format("({0, number})", ((EventModel) theEvent).getDisplayAge(theDate))));
		lstLabels.add(new Label(theEvent.getEventtype().getValue()));
		lstLabels.add(new Label(theEvent.getTitle().getValue()));
		
		if (LocalPrefs.getBoolean(PrefKey.DISPLAY_CATEGORIES)) {
			lstLabels.add(new Label(theEvent.getCategory().getValue()));
		}
		
		List<AnchorPane> lstPanes = new ArrayList<>();
		
		lstLabels.stream().forEach(label -> {
			
			label.setStyle(String.format("-fx-text-fill: %1$s; -fx-background-color: %2$s; -fx-border-color: white; -fx-font-size: %3$d; -fx-text-alignment: center; -fx-alignment: center; -fx-padding: 10;",
					LocalPrefs.get(PrefKey.fromValue(String.format("%s_foreground", theTimeCategory.value()))),
					LocalPrefs.get(PrefKey.fromValue(String.format("%s_background", theTimeCategory.value()))),
					LocalPrefs.getInt(PrefKey.fromValue(String.format("%s_fontsize", theTimeCategory.value())))
					));
			
			AnchorPane pneTemp = new AnchorPane(label);
			AnchorPane.setTopAnchor(label, 0.0);
			AnchorPane.setRightAnchor(label, 0.0);
			AnchorPane.setBottomAnchor(label, 0.0);
			AnchorPane.setLeftAnchor(label, 0.0);
			lstPanes.add(pneTemp);
			
		});
		
		grdEvents.addRow(grdEvents.getRowCount(), lstPanes.toArray(new AnchorPane[lstPanes.size()]));
		
		RowConstraints rowConstraints = new RowConstraints();
		rowConstraints.setValignment(VPos.TOP);
		grdEvents.getRowConstraints().add(rowConstraints);
		
	}

	/**
	 * Handles pressing of ESC, closes app.
	 */
	@FXML
	private void keyListener(KeyEvent event){
		if (event.getCode() == KeyCode.ESCAPE) {
			appController.handleProgramExit();
			// this code is reached only if program was not exited, thus consume event
			event.consume();
		}
	}

}

/* EOF */
