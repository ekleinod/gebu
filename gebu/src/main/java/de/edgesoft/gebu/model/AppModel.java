package de.edgesoft.gebu.model;

import java.nio.file.Paths;

import de.edgesoft.gebu.jaxb.Gebu;
import de.edgesoft.gebu.utils.PrefKey;
import de.edgesoft.gebu.utils.LocalPrefs;

/**
 * Gebu application model.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
public final class AppModel {

	/**
	 * Gebu event data.
	 */
	private static Gebu dtaGebu = null;

	/**
	 * Flag if data is modified.
	 */
	private static boolean isModified = false;

	/**
	 * Flag if data is legacy.
	 */
	private static boolean isLegacy = false;

	/**
     * Returns gebu data.
     *
     * @return gebu data
     */
    public static Gebu getData() {
        return dtaGebu;
    }

	/**
     * Sets gebu data.
     *
     * @return gebu data
     */
    public static void setData(final Gebu theData) {
        dtaGebu = theData;
    }

	/**
     * Is data modified?.
     *
     * @return Is data modified?
     */
    public static boolean isModified() {
        return isModified;
    }

	/**
     * Sets modified flag.
     *
     * @param modified data modified?
     */
    public static void setModified(final boolean modified) {
        isModified = modified;
    }

	/**
     * Is data legacy?.
     *
     * @return Is data legacy?
     */
    public static boolean isLegacy() {
        return isLegacy;
    }

	/**
     * Sets legacy flag.
     *
     * @param legacy data legacy?
     */
    public static void setLegacy(final boolean legacy) {
    	isLegacy = legacy;
    }

	/**
	 * Returns the file name.
	 *
	 * @return filename
	 */
	public static String getFilename() {
		return LocalPrefs.get(PrefKey.FILE);
    }

	/**
	 * Sets the file name.
	 *
	 * @param theFilename filename
	 */
	public static void setFilename(final String theFilename) {

		if (theFilename == null) {
			LocalPrefs.put(PrefKey.FILE, "");
		} else {
			LocalPrefs.put(PrefKey.FILE, theFilename);
			LocalPrefs.put(PrefKey.PATH, Paths.get(theFilename).getParent().toString());
		}

    }

}

/* EOF */
