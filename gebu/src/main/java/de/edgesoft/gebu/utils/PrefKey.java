package de.edgesoft.gebu.utils;

import javafx.stage.Screen;

/**
 * Preference keys.
 *
 * For enums I use the coding style of jaxb, so there will be no inconsistencies.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
public enum PrefKey {

	DISPLAY_CATEGORIES(Boolean.FALSE.toString()),

	FILE,

	FUTURE_BACKGROUND("#F0F8FF"),
	FUTURE_FONTSIZE("10"),
	FUTURE_FOREGROUND("#6495ED"),

	INTERVAL("7"),

	MAXIMIZED(Boolean.FALSE.toString()),

	PAST_BACKGROUND("#F5F5F5"),
	PAST_FONTSIZE("10"),
	PAST_FOREGROUND("#008080"),

	PATH,
	
	PREFERENCES_FILE,

	PRESENT_BACKGROUND("#FFFFFF"),
	PRESENT_FONTSIZE("12"),
	PRESENT_FOREGROUND("#D2691E"),

	SIZE_FACTOR(Double.toString(1)),

	STAGE_HEIGHT(Double.toString(600)),
	STAGE_X(Double.toString((Screen.getPrimary().getBounds().getWidth() - 800) / 2)),
	STAGE_WIDTH(Double.toString(800)),
	STAGE_Y(Double.toString((Screen.getPrimary().getBounds().getHeight() - 600) / 2)),
	STAGE_SPLIT(Double.toString(0.6)),

	TITLE_FULLPATH(Boolean.FALSE.toString()),
	;

    private final String value;

    private final String defaultValue;

    PrefKey() {
        value = name().toLowerCase();
        defaultValue = "";
    }

    PrefKey(final String theDefault) {
        value = name().toLowerCase();
        defaultValue = theDefault;
    }

    public String value() {
        return value;
    }

    public static PrefKey fromValue(String v) {
        for (PrefKey c: PrefKey.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public String defaultValue() {
        return defaultValue;
    }

}

/* EOF */
