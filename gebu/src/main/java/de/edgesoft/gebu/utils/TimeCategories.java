
package de.edgesoft.gebu.utils;

import java.util.OptionalInt;

/**
 * Time categories.
 *
 * For enums I use the coding style of jaxb, so there will be no inconsistencies.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
public enum TimeCategories {

	PAST(OptionalInt.empty(), OptionalInt.of(1), -1),
	PRESENT(OptionalInt.of(0), OptionalInt.of(0), 1),
	FUTURE(OptionalInt.of(1), OptionalInt.empty(), 1),
	;

    private final String value;
    private final OptionalInt iIntervalStart;
    private final OptionalInt iIntervalEnd;
    private final int iSign;

    TimeCategories(final OptionalInt theIntervalStart, final OptionalInt theIntervalEnd, final int theSign) {
        value = name().toLowerCase();
        iIntervalStart = theIntervalStart;
        iIntervalEnd = theIntervalEnd;
        iSign = theSign;
    }

    /**
     * Returns start interval, empty for preferences interval.
     * 
     * @return start interval
     */
    public OptionalInt getIntervalStart() {
        return iIntervalStart;
    }

    /**
     * Returns end interval, empty for preferences interval.
     * 
     * @return end interval
     */
    public OptionalInt getIntervalEnd() {
        return iIntervalEnd;
    }

    /**
     * Returns sign of interval.
     * 
     * @return sign
     */
    public int getSign() {
        return iSign;
    }

    /**
     * Returns value = lowercase name.
     * 
     * @return value
     */
    public String value() {
        return value;
    }

    public static TimeCategories fromValue(String v) {
        for (TimeCategories c: TimeCategories.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}

/* EOF */
