package de.edgesoft.gebu.utils;

import de.edgesoft.edgeutils.files.Prefs;

/**
 * Central preferences class.
 *
 * <h3>Legal stuff</h3>
 *
 * <p>Copyright 2016-2022 Ekkart Kleinod <a href="mailto:ekleinod@edgesoft.de">ekleinod@edgesoft.de</a></p>
 *
 * <p>This file is part of "Das Gebu-Programm".</p>
 *
 * <p>"Das Gebu-Programm" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.</p>
 *
 * <p>"Das Gebu-Programm" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU General Public License
 * along with "Das Gebu-Programm".  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>
 *
 * @author Ekkart Kleinod
 * @since 6.0.0
 */
public class LocalPrefs extends Prefs {

	/**
	 * Get preference for key.
	 *
	 * @param theKey preference key
	 * @return preference value
	 */
	public static String get(
			final PrefKey theKey
			) {
		
		return get(theKey.value(), theKey.defaultValue());

	}

	/**
	 * Get preference for key as double value.
	 *
	 * @param theKey preference key
	 * @return preference value as double
	 */
	public static double getDouble(
			final PrefKey theKey
			) {
		return getPrefs().getDouble(theKey.value(), Double.parseDouble(theKey.defaultValue()));
	}

	/**
	 * Get preference for key as int value.
	 *
	 * @param theKey preference key
	 * @return preference value as int
	 */
	public static int getInt(
			final PrefKey theKey
			) {
		return getPrefs().getInt(theKey.value(), Integer.parseInt(theKey.defaultValue()));
	}

	/**
	 * Get preference for key as boolean value.
	 *
	 * @param theKey preference key
	 * @return preference value as boolean
	 */
	public static boolean getBoolean(
			final PrefKey theKey
			) {
		return getPrefs().getBoolean(theKey.value(), Boolean.parseBoolean(theKey.defaultValue()));
	}

	/**
	 * Set preference value for key.
	 *
	 * @param theKey preference key
	 * @param theValue value
	 */
	public static void put(
			final PrefKey theKey,
			final String theValue
			) {
		put(theKey.value(), theValue);
	}

	/**
	 * Set double preference value for key.
	 *
	 * @param theKey preference key
	 * @param theValue value
	 */
	public static void putDouble(
			final PrefKey theKey,
			final double theValue
			) {
		getPrefs().putDouble(theKey.value(), theValue);
	}

	/**
	 * Set int preference value for key.
	 *
	 * @param theKey preference key
	 * @param theValue value
	 */
	public static void putInt(
			final PrefKey theKey,
			final int theValue
			) {
		getPrefs().putInt(theKey.value(), theValue);
	}

	/**
	 * Set boolean preference value for key.
	 *
	 * @param theKey preference key
	 * @param theValue value
	 */
	public static void putBoolean(
			final PrefKey theKey,
			final boolean theValue
			) {
		getPrefs().putBoolean(theKey.value(), theValue);
	}

}

/* EOF */
