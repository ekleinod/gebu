# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [6.0.0] - 2022-06-13

### Added

- deb installer
- windows installer with innosetup
- user manual with mkdocs
- new preference: scale of icons
- css styles
- sbom file

### Changed

- rewrote event and statistics display, now using freemarker
- new icon and graphics
- switch to gitlab
- switch to modules (Java 9+)
- GPL instead of LGPL
- svg icons instead of png
- display with JavaFX instead of freemarker
- file extension is not added automatically anymore

### Fixed

- age computation start/end of year
- order of events
- wrong input in datepicker
- javadoc formatting


## [6.0.0 beta 3 installer] - 2016-10-25

### Fixed

- Windows installer, now working on Windows 7


## [6.0.0 beta 3] - 2016-10-24

### Added

- Windows installer
- new preference: filename in title short or full path
- new preference: category display selectable

### Changed

- new icon and graphics
- completed MVC concept
- file extension ".gebu" instead of ".esx"
- links in about dialog clickable

### Fixed

- date input without leading zeros
- more stable table view after saving and editing
- event display did not show all events


## [6.0.0 beta 2] - 2016-10-03

### Added

- beloved Escape functionality (close program with ESC key)
- file extensions ".esx" is added if no extension is given

### Changed

- improved documentation

### Fixed

- new and deleted data was not saved
- NullPointerException when filtering events without category


## [6.0.0 beta 1] - 2016-10-03

- complete rewrite using JavaFX
- new XML format
- imports data from version 5.0.0


## [5.0.0]

- Java version
- XML data format
- model-view-controller
- imports data from version 4.0.0


## [4.0.0]

- initial Java version
- ASCII data format
- converter for data of version 3.0.0


## [3.0.0]

- the classic Windows version
- written in Visual Basic
- integrated editor
- binary data format
- problems running under Windows 7+


## [2.0.0]

- never finished rewrite for Windows


## [1.0.0]

- DOS version
- show data but no edit functionality
