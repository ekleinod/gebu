; -- gebu.iss --

; Das Gebu-Programm
; Copyright 2016-2022 Ekkart Kleinod <ekleinod@edgesoft.de>
;
; The program is distributed under the terms of the GNU General Public License.
;
; See COPYING for details.
;
; This file is part of "Das Gebu-Programm".
;
; "Das Gebu-Programm" is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; "Das Gebu-Programm" is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with "Das Gebu-Programm".  If not, see <http://www.gnu.org/licenses/>.
;
; @author Ekkart Kleinod

; ----------------
; Basic setup of the installer: names, directories, privileges
[Setup]
; update for new version here if needed
#define MyAppName "Das Gebu-Programm"
#define MyAppVersion "6.0.0"
#define MyAppCopyright "Copyright 2016-2022 Ekkart Kleinod <ekleinod@edgesoft.de>"
#define MyAppPublisher "Ekkart Kleinod"

#define MyStartmenuGroup "Das Gebu-Programm"

#define MyJavaFXVersion "18.0.1"
#define MyJavaJDKVersion "18.0.1"

#define MyInstallDir "gebu"
#define MyInstallerFile "gebu-install_" + MyAppVersion
; all lines below should not be changed


AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppCopyright={#MyAppCopyright}
OutputBaseFilename={#MyInstallerFile}
DefaultGroupName={#MyAppName}

AppComments={cm:SetupAppComments}
AppPublisher={#MyAppPublisher}

SourceDir=install-files
; relative to source dir
OutputDir=..\installer

DefaultDirName={autopf}\{#MyInstallDir}
; enable user to deselect group creation in start menu
AllowNoIcons=yes

WizardStyle=modern
PrivilegesRequired=lowest
DisableWelcomePage=no
; just for testing, delete for production
Compression=none


; ----------------
; Installer language
[Languages]
; relative to source dir
Name: "de"; MessagesFile: "compiler:Languages\German.isl"
Name: "en"; MessagesFile: "compiler:Default.isl"


; ----------------
; Translated messages
[CustomMessages]
de.SetupAppComments={#MyAppName} zeigt Geburtstage in einem Rahmen von zwei Wochen an, d.h. die Geburtstage der vergangenen Woche, die heutigen Geburtstage sowie die zukünftigen Geburtstage der nächsten Woche.
de.TypesFull=Komplette Installation
de.TypesNoJava=Installation ohne Java-JDK und JavaFX-SDK
de.TypesCustom=Angepasste Installation
de.ComponentsGebu=Gebu-Programmdateien
de.ComponentsJava=Java-JDK, startet {#MyAppName}
de.ComponentsJavaFX=JavaFX-SDK, zeigt die Bedienoberfläche von {#MyAppName} an
de.TaskAutostart={#MyAppName} automatisch beim Rechnerstart starten?
de.IconStart={#MyAppName} starten.
de.IconAutoStart={#MyAppName} automatisch beim Rechnerstart starten.

en.SetupAppComments={#MyAppName} shows birthdays in a two week interval, i.e. birthdays of the last week, the actual birthdays, and the birthdays of the future week.
en.TypesFull=Full Installation
en.TypesNoJava=Installation without Java-JDK and JavaFX-SDK
en.TypesCustom=Custom Installation
en.ComponentsGebu={#MyAppName} program files
en.ComponentsJava=Java-JDK, starts {#MyAppName}
en.ComponentsJavaFX=JavaFX-SDK, show the UI of {#MyAppName}
en.TaskAutostart=Start {#MyAppName} when starting the computer?
en.IconStart=Start {#MyAppName}.
en.IconAutoStart=Start {#MyAppName} when starting the computer.

; ----------------
; predefined installation types selectable by the user
; (use custom flag for allowing custom installs)
[Types]
Name: "full"; Description: "{cm:TypesFull}"
Name: "nojava"; Description: "{cm:TypesNoJava}"
Name: "custom"; Description: "{cm:TypesCustom}"; Flags: iscustom


; ----------------
; (de)selectable components
[Components]
Name: "cmp_gebu"; Description: "{cm:ComponentsGebu}"; Types: full nojava custom; Flags: fixed
Name: "cmp_java"; Description: "{cm:ComponentsJava}"; Types: full custom
Name: "cmp_javafx"; Description: "{cm:ComponentsJavaFX}"; Types: full custom


; ----------------
; user customizable tasks
[Tasks]
Name: "task_autostart"; Description: "{cm:TaskAutostart}"; Flags: unchecked


; ----------------
; files (and directories) that are installed
[Files]
Source: "gebu.jar"; DestDir: "{app}"; Components: cmp_gebu
Source: "gebu.ico"; DestDir: "{app}"; Components: cmp_gebu
Source: "JavaFX-SDK-{#MyJavaFXVersion}\*"; DestDir: "{app}\JavaFX-SDK-{#MyJavaFXVersion}"; Flags: recursesubdirs; Components: cmp_javafx
Source: "Java-JDK-{#MyJavaJDKVersion}\*"; DestDir: "{app}\Java-JDK-{#MyJavaJDKVersion}"; Flags: recursesubdirs; Components: cmp_java


; ----------------
; shortcuts that are created, e.g. in start menu, or desktop
[Icons]
; java and javafx will be installed
Name: "{group}\{#MyStartmenuGroup}"; Filename: "{app}\Java-JDK-{#MyJavaJDKVersion}\bin\javaw.exe"; Parameters: "--module-path {app}\JavaFX-SDK-{#MyJavaFXVersion}\lib\ --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.web -jar ""{app}\gebu.jar"""; Comment: "{cm:IconStart}"; WorkingDir: "{app}"; IconFileName: "{app}\gebu.ico"; Components: cmp_gebu and (cmp_java and cmp_javafx)
Name: "{autostartup}\{#MyStartmenuGroup}"; Filename: "{app}\Java-JDK-{#MyJavaJDKVersion}\bin\javaw.exe"; Parameters: "--module-path {app}\JavaFX-SDK-{#MyJavaFXVersion}\lib\ --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.web -jar ""{app}\gebu.jar"""; Comment: "{cm:IconAutoStart}"; WorkingDir: "{app}"; IconFileName: "{app}\gebu.ico"; Components: cmp_gebu and (cmp_java and cmp_javafx); Tasks: task_autostart

; only java is installed
Name: "{group}\{#MyStartmenuGroup}"; Filename: "{app}\Java-JDK-{#MyJavaJDKVersion}\bin\javaw.exe"; Parameters: "--module-path %PATH_TO_FX%\lib\ --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.web -jar ""{app}\gebu.jar"""; Comment: "{cm:IconStart}"; WorkingDir: "{app}"; IconFileName: "{app}\gebu.ico"; Components: (cmp_gebu and cmp_java) and not cmp_javafx
Name: "{autostartup}\{#MyStartmenuGroup}"; Filename: "{app}\Java-JDK-{#MyJavaJDKVersion}\bin\javaw.exe"; Parameters: "--module-path %PATH_TO_FX%\lib\ --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.web -jar ""{app}\gebu.jar"""; Comment: "{cm:IconAutoStart}"; WorkingDir: "{app}"; IconFileName: "{app}\gebu.ico"; Components: (cmp_gebu and cmp_java) and not cmp_javafx; Tasks: task_autostart

; only javafx is installed
Name: "{group}\{#MyStartmenuGroup}"; Filename: "{%JAVA_HOME}\bin\javaw.exe"; Parameters: "--module-path {app}\JavaFX-SDK-{#MyJavaFXVersion}\lib\ --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.web -jar ""{app}\gebu.jar"""; Comment: "{cm:IconStart}"; WorkingDir: "{app}"; IconFileName: "{app}\gebu.ico"; Components: (cmp_gebu and cmp_javafx) and not cmp_java
Name: "{autostartup}\{#MyStartmenuGroup}"; Filename: "{%JAVA_HOME}\bin\javaw.exe"; Parameters: "--module-path {app}\JavaFX-SDK-{#MyJavaFXVersion}\lib\ --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.web -jar ""{app}\gebu.jar"""; Comment: "{cm:IconAutoStart}"; WorkingDir: "{app}"; IconFileName: "{app}\gebu.ico"; Components: (cmp_gebu and cmp_javafx) and not cmp_java; Tasks: task_autostart

; neither java nor javafx are installed
Name: "{group}\{#MyStartmenuGroup}"; Filename: "{%JAVA_HOME}\bin\javaw.exe"; Parameters: "--module-path %PATH_TO_FX%\lib\ --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.web -jar ""{app}\gebu.jar"""; Comment: "{cm:IconStart}"; WorkingDir: "{app}"; IconFileName: "{app}\gebu.ico"; Components: cmp_gebu and not (cmp_javafx or cmp_java)
Name: "{autostartup}\{#MyStartmenuGroup}"; Filename: "{%JAVA_HOME}\bin\javaw.exe"; Parameters: "--module-path %PATH_TO_FX%\lib\ --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.web -jar ""{app}\gebu.jar"""; Comment: "{cm:IconAutoStart}"; WorkingDir: "{app}"; IconFileName: "{app}\gebu.ico"; Components: cmp_gebu and not (cmp_javafx or cmp_java); Tasks: task_autostart
