# Das Gebu-Programm

Das Gebu-Programm zeigt Geburtstage in einem Rahmen von zwei Wochen an, d.h. die Geburtstage der vergangenen Woche, die heutigen Geburtstage sowie die zukünftigen Geburtstage der nächsten Woche.
Die Geburtstage können mit einem internen Editor erweitert oder geändert werden.
Das Anzeigeintervall ist einstellbar.

"Das Gebu-Programm" shows birthdays in a two week interval, i.e. birthdays of the last week, the actual birthdays, and the birthdays of the future week.
The birthdays can be edited with an internal editor.
The interval is changeable.

Einen Überblick über alle Änderungen finden Sie im [changelog](changelog.md).

For an overview of all changes, see [changelog](changelog.md).


## Bedienung

Es gibt ein wunderschönes Handbuch:

- [Gebu-Handbuch](https://ekleinod.gitlab.io/gebu/)

Das Programm kann von Sourceforge heruntergeladen werden:

- https://sourceforge.net/projects/gebu/
- https://sourceforge.net/projects/gebu/files/latest/download


## Usage

There is a beautiful manual:

- [Gebu manual](https://ekleinod.gitlab.io/gebu/en/)

Download the program from sourceforge:

- https://sourceforge.net/projects/gebu/
- https://sourceforge.net/projects/gebu/files/latest/download



## Nutzung anderer Arbeit

Alle externen Module, sind in der `pom.xml` aufgeführt/ all external modules are listed in `pom.xml`:

- [pom.xml](gebu/pom.xml)

Die Icons sind aus dem Papirus Icon Theme oder FreeSVG entnommen.
Fehlende Icons habe ich selbst erschaffen oder modifiziert.

The icons are from the Papirus Icon Theme or from FreeSVG.
I created missing icons myself or modified existing ones.

- https://github.com/PapirusDevelopmentTeam/papirus-icon-theme-gtk
- https://freesvg.org


## Git-Repository

Das Branching-Modell folgt den Empfehlungen zum *Stable Mainline Model* von <http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git>.

Das heißt, es gibt immer einen stabilen Branch: `main`.
Dieser Branch ist immer fehlerfrei test- und kompilierbar.

The branching model regards to the stable mainline model described in <http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git>

This means, there is always a stable mainline, the `main` branch.
This branch ist always compileable and testable, both without errors.

For more details, see [developer documentation](https://refereemanager.readthedocs.io/de/latest/developer/structure.html).

## Copyright

Copyright 2016-2022 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License.

See COPYING for details.

This file is part of "Das Gebu-Programm".

"Das Gebu-Programm" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

"Das Gebu-Programm" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "Das Gebu-Programm".  If not, see <http://www.gnu.org/licenses/>.
